/*
 * Copyright(c) 1992 Bell Communications Research, Inc. (Bellcore)
 * Copyright(c) 1995-99 Andrew Lister
 * Copyright � 1999, 2000, 2001, 2002, 2003, 2004 by the LessTif Developers.
 *
 *                        All rights reserved
 * Permission to use, copy, modify and distribute this material for
 * any purpose and without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies, and that the name of Bellcore not be used in advertising
 * or publicity pertaining to this material without the specific,
 * prior written permission of an authorized representative of
 * Bellcore.
 *
 * BELLCORE MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES, EX-
 * PRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST IN-
 * FRINGEMENT OF PATENTS OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE
 * SOFTWARE IS PROVIDED "AS IS", AND IN NO EVENT SHALL BELLCORE OR
 * ANY OF ITS AFFILIATES BE LIABLE FOR ANY DAMAGES, INCLUDING ANY
 * LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES RELAT-
 * ING TO THE SOFTWARE.
 *
 * $Id: Actions.c,v 1.114 2006/05/16 19:59:53 tobiasoed Exp $
 */

#define	VERBOSE_SLIDE

/*
 * Actions.c created by Andrew Lister (7 August, 1995)
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <Xm/Xm.h>
#include <Xm/XmP.h>
#include <Xm/DrawP.h>
#include <Xm/ScrollBar.h>
#include <Xbae/MatrixP.h>
#include <Xbae/Draw.h>
#include <Xbae/Actions.h>
#include <Xbae/Utils.h>
#include <Xbae/ScrollMgr.h>
#include <X11/cursorfont.h>

#include <XbaeDebug.h>

#ifndef XlibSpecificationRelease
#define XrmPermStringToQuark XrmStringToQuark
#endif

/* One of DRAW_RESIZE_LINE and DRAW_RESIZE_SHADOW must be defined. */
#if !defined(DRAW_RESIZE_LINE) && !defined(DRAW_RESIZE_SHADOW)
#define DRAW_RESIZE_SHADOW
#endif

#ifndef DEFAULT_SCROLL_SPEED
#define DEFAULT_SCROLL_SPEED 30
#endif

/*
 * This is the number of pixels that you can 'miss' the cell edge when
 * trying to resize a row or column.
 */
#ifndef XBAE_RESIZE_FUZZ
#define XBAE_RESIZE_FUZZ	4
#endif

typedef struct {
        XbaeMatrixWidget mw;
        XtIntervalId timerID;
        XtAppContext app_context;
        void (*fcn)(XtPointer data);
        Boolean grabbed;
        int pointer_x;
        int pointer_y;
} XbaeMatrixPositionStruct;

typedef struct {
        XbaeMatrixPositionStruct pos;
        Boolean resize_row;
        Boolean resize_column;
        int row; 
        int column;
        int currentx;
        int currenty;
        short *columnWidths, *rowHeights;
} XbaeMatrixResizeStruct;

typedef struct {
        XbaeMatrixPositionStruct pos;
        Boolean pressed;
        int initial_region;
        int label_row;
        int label_column;
        int distx;
        int disty;
} XbaeMatrixButtonPressedStruct;

typedef struct {
        XbaeMatrixPositionStruct pos;
        XEvent *event;
        Cardinal num_params;
        String *params;
        int initial_region;
        int last_row;
        int last_column;
        int distx;
        int disty;
} XbaeMatrixScrollStruct;

static int DoubleClick(XbaeMatrixWidget, XEvent *, int, int);
static void DrawSlideRow(XbaeMatrixWidget, int);
static void DrawSlideColumn(XbaeMatrixWidget, int);
static void slideResize(XtPointer);
static void scrollSelect(XtPointer);
static void scrollLabel(XtPointer);
static void handleMotionEvent(Widget, XtPointer, XEvent *, Boolean *);
static void callSelectCellCallbacks(XbaeMatrixWidget, XEvent *, int, int, String *, Cardinal);

/**************************************************************************************************/

static int DoubleClick(XbaeMatrixWidget mw, XEvent * event, int row, int column)
{
        /* A double click in this instance is two clicks in the
           same cell in a time period < double_click_interval */
        Time current_time;
        unsigned long delta;
        static int ret = 0;
        static int lastButton = 0;

        if (event->type == ButtonRelease) {
                /* If the button is released, store the current location and time -
                   next time through, if it's a button press event, we check for
                   double click */
                mw->matrix.last_row = row;
                mw->matrix.last_column = column;
                if (ret)        /* just had a double click */
                        mw->matrix.last_click_time = (Time) 0;
                else
                        mw->matrix.last_click_time = event->xbutton.time;
                ret = 0;

                lastButton = event->xbutton.button;

                return ret;
        }

        current_time = event->xbutton.time;
        delta = current_time - mw->matrix.last_click_time;

        if (row == mw->matrix.last_row && column == mw->matrix.last_column
            && delta < (unsigned long) mw->matrix.double_click_interval
            && event->xbutton.button == lastButton) {
                ret = 1;
        } else {
                ret = 0;
        }

        return ret;
}

/* ARGSUSED */
void xbaeDefaultActionACT(Widget w, XEvent * event, String * params, Cardinal * nparams)
{
        XbaeMatrixWidget mw;
        int x, y;
        int row, column;

        /*
         * Get the Matrix widget. w could be the TextField one of the 
         * cellWidgets or the Matrix itself
         */
        mw = xbaeEventToMatrixWidget(w, event);
        if (!mw) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "defaultActionACT", "badWidget",
                                "XbaeMatrix",
                                "XbaeMatrix: Bad widget passed to DefaultAction action", NULL, 0);
                return;
        }

        if (!mw->matrix.default_action_callback) {
                return;
        }

        if (!xbaeEventToRowColumn(w, event, &row, &column, &x, &y)) {
                return;
        }

        DEBUGOUT(_XbaeDebug
                 (__FILE__, w, "xbaeDefaultActionACT(x %d, y %d) on %s with button %s\n", 
                  x, y,
                  mw == (XbaeMatrixWidget) w ? "matrix" : "other",
                  event->type == ButtonRelease ? "release" : "press"));

        if (DoubleClick(mw, event, row, column)) {
                XbaeMatrixDefaultActionCallbackStruct call_data;

                call_data.reason = XbaeDefaultActionReason;
                call_data.event = event;
                call_data.row = row;
                call_data.column = column;

                XtCallCallbackList((Widget) mw, mw->matrix.default_action_callback,
                                   (XtPointer) & call_data);
        }
}

/**************************************************************************************************/

static void
callTraverseCellCallbacks(XbaeMatrixWidget mw, XEvent * event, int *row, int *column, String param,
                        XrmQuark qparam)
{
        /*
         * Call the traverseCellCallback to allow the application to
         * perform custom traversal.
         */
        XbaeMatrixTraverseCellCallbackStruct call_data;

        call_data.reason = XbaeTraverseCellReason;
        call_data.event = event;
        call_data.next_row = *row;
        call_data.next_column = *column;
        call_data.fixed_rows = mw->matrix.fixed_rows;
        call_data.fixed_columns = mw->matrix.fixed_columns;
        call_data.trailing_fixed_rows = mw->matrix.trailing_fixed_rows;
        call_data.trailing_fixed_columns = mw->matrix.trailing_fixed_columns;
        call_data.num_rows = mw->matrix.rows;
        call_data.num_columns = mw->matrix.columns;
        call_data.param = param;
        call_data.qparam = qparam;
        XtVaGetValues(TextField(mw), XmNattachRow, &call_data.row, XmNattachColumn, &call_data.column, NULL);

        XtCallCallbackList((Widget) mw, mw->matrix.traverse_cell_callback,
                           (XtPointer) & call_data);

        *row = call_data.next_row;
        *column = call_data.next_column;
}

/*
 * Action to edit cell.
 */
void xbaeEditCellACT(Widget w, XEvent * event, String * params, Cardinal * nparams)
{
        XbaeMatrixWidget mw;
	XbaeMatrixWidgetClass mwc;
        int x, y;
        int row, column;
        XrmQuark q;

        /*
         * Get the Matrix widget. w could be the TextField one of the 
         * cellWidgets or the Matrix itself
         */
        mw = xbaeEventToMatrixWidget(w, event);
        if (!mw) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "editCellACT", "badWidget",
                                "XbaeMatrix", "XbaeMatrix: Bad widget passed to EditCell action",
                                NULL, 0);
                return;
        }

        if (!xbaeEventToRowColumn(w, event, &row, &column, &x, &y)) {
                return;
        }
        
        /*
         * Make sure we have a single parm
         */
        if (*nparams != 1) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "editCellACT", "badParms",
                                "XbaeMatrix",
                                "XbaeMatrix: Wrong params passed to EditCell action, needs 1", NULL,
                                0);
                return;
        }
        
        /*
         * Quarkify the string param
         */
        q = XrmStringToQuark(params[0]);
	mwc = (XbaeMatrixWidgetClass) XtClass(mw);

        if (q == mwc->matrix_class.QPointer) {
                if (row < 0 || column < 0 || 
                    (IS_FIXED(mw, row, column) && !mw->matrix.traverse_fixed)) {
                        return;
                }
        } else {
                /*
                 * Make sure row/column are sane
                 */
                int next_row, first_row, last_row;
                int next_column, first_column, last_column;

                if (row < 0 || row >= mw->matrix.rows) {
                        row = xbaeTopRow(mw);
                }
                if (column < 0 || column >= mw->matrix.columns) {
                        column = xbaeLeftColumn(mw);
                }

                next_row = row;
                next_column = column;
                
                if (mw->matrix.traverse_fixed) {
                        first_row = 0;
                        first_column = 0;
                        last_row = mw->matrix.rows - 1;
                        last_column = mw->matrix.columns - 1;
                } else {
                        first_row = mw->matrix.fixed_rows;
                        first_column = mw->matrix.fixed_columns;
                        last_row = TRAILING_ROW_ORIGIN(mw) - 1;
                        last_column = TRAILING_COLUMN_ORIGIN(mw) - 1;
                }
                
                if (q == mwc->matrix_class.QRight) {
                        /*
                         * If we are in the lower right corner, stay there.
                         * Otherwise move over a column. If we move off to the right of the
                         * final column to which traversing is allowed then move down a row
                         * and back to the first column to which traversing is allowed.
                         */
                        
                        for(next_column++;
                            next_column <= last_column && COLUMN_WIDTH(mw, next_column) == 0;
                            next_column++)
                                ;
                        
                        if (next_column > last_column) {
                                for(next_row++;
                                    next_row <= last_row && ROW_HEIGHT(mw, next_row) == 0; 
                                    next_row++)
                                        ;
                                
                                if (next_row <= last_row) {
                                        for(next_column = first_column; 
                                            next_column <= last_column && COLUMN_WIDTH(mw, next_column) == 0; 
                                            next_column++)
                                                ;
                                }
                        }
                } else if (q == mwc->matrix_class.QLeft) {
                        /*
                         * If we are in the upper left corner, stay there.
                         * Otherwise move back a column. If we move before the first column
                         * to which traversing is allowed, move up a row and over to the last
                         * column to which traversing is allowed.
                         */

                        for(next_column--;
                            next_column >= first_column && COLUMN_WIDTH(mw, next_column) == 0;
                            next_column--)
                                ;
                        
                        if (next_column < first_column) {
                                for(next_row--;
                                    next_row >= first_row && ROW_HEIGHT(mw, next_row) == 0; 
                                    next_row--)
                                        ;
                                
                                if (next_row >= first_row) {
                                        for(next_column = last_column; 
                                            next_column >= first_column && COLUMN_WIDTH(mw, next_column) == 0; 
                                            next_column--)
                                                ;
                                }
                        }
                 } else if (q == mwc->matrix_class.QDown) {

                        for(next_row++;
                            next_row <= last_row && ROW_HEIGHT(mw, next_row) == 0; 
                            next_row++)
                                ;

                        if (next_row > last_row) {
                                for(next_row = first_row;
                                    next_row <= last_row  && ROW_HEIGHT(mw, next_row) == 0; 
                                    next_row++)
                                        ;
                        }
                } else if (q == mwc->matrix_class.QUp) {
                        for(next_row--;
                            next_row >= first_row && ROW_HEIGHT(mw, next_row) == 0; 
                            next_row--)
                                ;

                        if (next_row < first_row) {
                                for(next_row = last_row;
                                    next_row >= first_row  && ROW_HEIGHT(mw, next_row) == 0; 
                                    next_row--)
                                        ;
                        }
                }
                
                if(next_row >= first_row && next_row <= last_row && 
                   next_column >= first_column && next_column <= last_column) {
                        row = next_row;
                        column = next_column;
                }
        }

        /*
         * Call the traverseCellCallback to allow the application to
         * perform custom traversal.
         */
        if (mw->matrix.traverse_cell_callback) {
                callTraverseCellCallbacks(mw, event, &row, &column, params[0], q);
        }

        ((XbaeMatrixWidgetClass) XtClass(mw))->matrix_class.edit_cell(mw, event, 
                                                                      row, column, 
                                                                      params, *nparams);
}

/*
 * Action to unmap the textField and discard any edits made
 */
/* ARGSUSED */
void xbaeCancelEditACT(Widget w, XEvent * event, String * params, Cardinal * nparams)
{
        XbaeMatrixWidget mw;
        Boolean unmap;

        /*
         * Get the Matrix widget. w could be the TextField one of the 
         * cellWidgets or the Matrix itself
         */
        mw = xbaeEventToMatrixWidget(w, event);
        if (!mw) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "cancelEditACT", "badWidget",
                                "XbaeMatrix", "XbaeMatrix: Bad widget passed to CancelEdit action",
                                NULL, 0);
                return;
        }

        /*
         * Make sure we have a single param
         */
        if (*nparams != 1) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "cancelEditACT", "badParms",
                                "XbaeMatrix",
                                "XbaeMatrix: Wrong params passed to CancelEdit action, needs 1",
                                NULL, 0);
                return;
        }

        /*
         * Validate our param
         */
        if (!strcmp(params[0], "True"))
                unmap = True;
        else if (!strcmp(params[0], "False"))
                unmap = False;
        else {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "cancelEditACT", "badParm",
                                "XbaeMatrix", "XbaeMatrix: Bad parameter for CancelEdit action",
                                NULL, 0);
                return;
        }
        
        if (!mw->matrix.text_field_is_mapped) {
                /*
                 * The TextField is not visible, call ManagerParentCancel
                 */
                XtCallActionProc((Widget) mw, "ManagerParentCancel", event, params, *nparams);
        } else {
                /*
                 * The TextField is visible, call the cancel_edit method
                 */
                ((XbaeMatrixWidgetClass) XtClass(mw))->matrix_class.cancel_edit(mw, unmap);
        }
}

/*
 * Action save any edits made and unmap the textField if params[0] is True
 */
/* ARGSUSED */
void xbaeCommitEditACT(Widget w, XEvent * event, String * params, Cardinal * nparams)
{
        XbaeMatrixWidget mw;
        Boolean unmap;

        /*
         * Get the Matrix widget. w could be the TextField one of the 
         * cellWidgets or the Matrix itself
         */
        mw = xbaeEventToMatrixWidget(w, event);
        if (!mw) {
               XtAppWarningMsg(XtWidgetToApplicationContext(w), "commitEditACT", "badWidget",
                                "XbaeMatrix", "XbaeMatrix: Bad widget passed to CommitEdit action",
                                NULL, 0);
                return;
        }

        /*
         * Make sure we have a single param
         */
        if (*nparams != 1) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "commitEditACT", "badParms",
                                "XbaeMatrix",
                                "XbaeMatrix: Wrong params for CommitEdit action, needs 1", NULL, 0);
                return;
        }

        /*
         * Validate our param
         */
        if (!strcmp(params[0], "True"))
                unmap = True;
        else if (!strcmp(params[0], "False"))
                unmap = False;
        else {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "commitEditACT", "badParm",
                                "XbaeMatrix", "XbaeMatrix: Bad parameter for CommitEdit action",
                                NULL, 0);
                return;
        }

        if (!mw->matrix.text_field_is_mapped) {
                /*
                 * The TextField is not visible, call ManagerParentActivate
                 */
                XtCallActionProc((Widget) mw, "ManagerParentActivate", event, params, *nparams);
        } else {
                /*
                 * The TextField is visible, call the commit_edit method
                 */
                ((XbaeMatrixWidgetClass) XtClass(mw))->matrix_class.commit_edit(mw, event, unmap);
        }
}

/* ARGSUSED */
void xbaePageDownACT(Widget w, XEvent * event, String * params, Cardinal * nparams)
{
        XbaeMatrixWidget mw;
        char *down = "0";
        int vert_origin;
        
        /*
         * Get the Matrix widget. w could be the TextField one of the 
         * cellWidgets or the Matrix itself
         */
        mw = xbaeEventToMatrixWidget(w, event);
        if (!mw) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "pageDownACT", "badWidget",
                                "XbaeMatrix", "XbaeMatrix: Bad widget passed to PageDown action",
                                NULL, 0);
                return;
        }

        if (!XtIsManaged(VertScrollChild(mw)))
                return;

        /*
         * Save the vert_origin - if scrolling occurs, the text widget needs
         * to be moved
         */
        vert_origin = VERT_ORIGIN(mw);
        
        XtCallActionProc(VertScrollChild(mw), "PageDownOrRight", event, &down, 1);

        if (VERT_ORIGIN(mw) != vert_origin) {
                /*
                 * Position the cursor at the top most non fixed row if there was
                 * a page down
                 */
                int row = xbaeTopRow(mw);
                int column;
		XrmQuark qparam = ((XbaeMatrixWidgetClass) XtClass(mw))->matrix_class.QPageDown;
                XtVaGetValues(TextField(mw), XmNattachColumn, &column, NULL);

                if (mw->matrix.traverse_cell_callback) {
                        callTraverseCellCallbacks(mw, event, &row, &column, XrmQuarkToString(qparam), qparam);
                }

                ((XbaeMatrixWidgetClass) XtClass(mw))->matrix_class.edit_cell(mw, event, 
                                                                              row, column, 
                                                                              params, *nparams);
        }
}

/* ARGSUSED */
void xbaePageUpACT(Widget w, XEvent * event, String * params, Cardinal * nparams)
{
        XbaeMatrixWidget mw;
        char *up = "0";
        int vert_origin;
        
        /*
         * Get the Matrix widget. w could be the TextField one of the 
         * cellWidgets or the Matrix itself
         */
        mw = xbaeEventToMatrixWidget(w, event);
        if (!mw) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "pageUpACT", "badWidget",
                                "XbaeMatrix", "XbaeMatrix: Bad widget passed to PageUp action",
                                NULL, 0);
                return;
        }

        if (!XtIsManaged(VertScrollChild(mw)))
                return;

        /*
         * Save the vert_origin - if scrolling occurs, the text widget needs
         * to be moved
         */
        vert_origin = VERT_ORIGIN(mw);

        XtCallActionProc(VertScrollChild(mw), "PageUpOrLeft", event, &up, 1);

        if (VERT_ORIGIN(mw) != vert_origin) {
                /*
                 * Position the cursor at the top most non fixed row if there was
                 * a page up
                 */
                int row = xbaeTopRow(mw);
                int column;
		XrmQuark qparam = ((XbaeMatrixWidgetClass) XtClass(mw))->matrix_class.QPageUp;
                XtVaGetValues(TextField(mw), XmNattachColumn, &column, NULL);

                if (mw->matrix.traverse_cell_callback) {
                        callTraverseCellCallbacks(mw, event, &row, &column, XrmQuarkToString(qparam), qparam);
                }

                ((XbaeMatrixWidgetClass) XtClass(mw))->matrix_class.edit_cell(mw, event, 
                                                                              row, column, 
                                                                              params, *nparams);
        }
}

/**************************************************************************************************/

/* ARGSUSED */
void xbaeHandleTrackingACT(Widget w, XEvent * event, String * params, Cardinal * nparams)
{
        XbaeMatrixTrackCellCallbackStruct call_data;

        XbaeMatrixWidget mw;
        int x, y;
        int row, column;

        DEBUGOUT(_XbaeDebug
                 (__FILE__, w, "%s(x %d y %d)\n", __FUNCTION__, event->xbutton.x,
                  event->xbutton.y));

        /*
         * Get the Matrix widget. w could be the TextField one of the 
         * cellWidgets or the Matrix itself
         */
        mw = xbaeEventToMatrixWidget(w, event);
        if (!mw) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "handleTrackingACT", "badWidget",
                                "XbaeMatrix",
                                "XbaeMatrix: Bad widget passed to HandleTracking action", NULL, 0);
                return;
        }

        if (!mw->matrix.track_cell_callback)
                return;

        xbaeEventToRowColumn(w, event, &row, &column, &x, &y);

        /* FIXME: pointer_xy used to be relative to the matrix. */
        call_data.pointer_x = x;
        call_data.pointer_y = y;
        
        if ((column != mw->matrix.prev_column) || (row != mw->matrix.prev_row)) {
                call_data.reason = XbaeTrackCellReason;
                call_data.event = event;
                call_data.prev_row = mw->matrix.prev_row;
                call_data.prev_column = mw->matrix.prev_column;
                call_data.row = row;
                call_data.column = column;

                /* printf ("%s %s %d  XY:%d %d RC: %d %d\n",
                 * __FILE__,__FUNCTION__,__LINE__,x, y, row, column);
                 */

                XtCallCallbackList((Widget) mw, mw->matrix.track_cell_callback,
                                   (XtPointer) & call_data);

                /*
                 * Merken
                 */
                mw->matrix.prev_column = column;
                mw->matrix.prev_row = row;
        }
}

/*
 * Action to process a drag out
 */
void xbaeProcessDragACT(Widget w, XEvent * event, String * params, Cardinal * nparams)
{
        XbaeMatrixWidget mw;
        XbaeMatrixCellValuesStruct cell_values;

        int x, y;
        int row, column;
        XbaeMatrixProcessDragCallbackStruct call_data;

        /*
         * Get the Matrix widget. w could be the TextField one of the 
         * cellWidgets or the Matrix itself
         */
        mw = xbaeEventToMatrixWidget(w, event);
        if (!mw) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "processDragACT", "badWidget",
                                "XbaeMatrix", "XbaeMatrix: Bad widget passed to ProcessDrag action",
                                NULL, 0);
                return;
        }

        if (!mw->matrix.process_drag_callback)
                return;

        if (!xbaeEventToRowColumn(w, event, &row, &column, &x, &y) || row < 0 || column < 0) {
                return;
        }
        
        xbaeGetCellValues(mw, row, column, False, &cell_values);

        call_data.type = cell_values.drawCB.type;
        call_data.string = cell_values.drawCB.string;
        call_data.pixmap = cell_values.drawCB.pixmap;
        call_data.mask = cell_values.drawCB.mask;
        
        call_data.reason = XbaeProcessDragReason;
        call_data.event = event;
        call_data.row = row;
        call_data.column = column;
        call_data.num_params = *nparams;
        call_data.params = params;

        XtCallCallbackList((Widget) mw, mw->matrix.process_drag_callback, (XtPointer) &call_data);
        
        if ((cell_values.drawCB.type & XbaeStringFree) == XbaeStringFree) {
                XtFree((XtPointer) cell_values.drawCB.string);
        }
}

void xbaeScrollRowsACT(Widget w, XEvent * event, String * params, Cardinal * nparams)
{
        XbaeMatrixWidget mw;
        int step;
        String end;

        /*
         * Get the Matrix widget. w could be the TextField one of the 
         * cellWidgets or the Matrix itself
         */
        mw = xbaeEventToMatrixWidget(w, event);
        if (!mw) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "scrollRowsAct", "badWidget",
                                "XbaeMatrix", "XbaeMatrix: Bad widget passed to ScrollRows action",
                                NULL, 0);
                return;
        }

        if (*nparams == 1) {
                step = strtol(params[0],&end,0);
        }
        
        if (*nparams != 1 || end == params[0]) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "scrollRowsAct", "badWidget",
                                "XbaeMatrix", "XbaeMatrix: Bad parameter passed to ScrollRows action",
                                NULL, 0);
                return;
        }                
    
        xbaeScrollRows(mw, step);
}

void xbaeScrollColumnsACT(Widget w, XEvent * event, String * params, Cardinal * nparams)
{
        XbaeMatrixWidget mw;
        int step;
        String end;

        /*
         * Get the Matrix widget. w could be the TextField one of the 
         * cellWidgets or the Matrix itself
         */
        mw = xbaeEventToMatrixWidget(w, event);
        if (!mw) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "scrollColumnsAct", "badWidget",
                                "XbaeMatrix", "XbaeMatrix: Bad widget passed to ScrollColumns action",
                                NULL, 0);
                return;
        }

        if (*nparams == 1) {
                step = strtol(params[0],&end,0);
        }
        
        if (*nparams != 1 || end == params[0]) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "scrollColumnssAct", "badWidget",
                                "XbaeMatrix", "XbaeMatrix: Bad parameter passed to ScrollColumns action",
                                NULL, 0);
                return;
        }

        xbaeScrollColumns(mw, step);
}

/* ARGSUSED */
/*
 * Dean Phillips needs xbaeTraversePrev/NextACT because ManagerGadgetNext/PrevTabGroup()
 * cause NutCracker's Motif implementation to crash.
 */
void
xbaeTraverseNextACT(Widget w, XEvent * event, String * params, Cardinal * nparams)
{
        XbaeMatrixWidget mw;

        /*
         * Get the Matrix widget. w could be the TextField one of the 
         * cellWidgets or the Matrix itself
         */
        mw = xbaeEventToMatrixWidget(w, event);
        if (!mw) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "traverseNextACT", "badWidget",
                                "XbaeMatrix",
                                "XbaeMatrix: Bad widget passed to TraverseNext action", NULL, 0);
                return;
        }
        
        XmProcessTraversal(TextField(mw), XmTRAVERSE_NEXT_TAB_GROUP);
}

/* ARGSUSED */
void
xbaeTraversePrevACT(Widget w, XEvent * event, String * params, Cardinal * nparams)
{
        XbaeMatrixWidget mw;

        /*
         * Get the Matrix widget. w could be the TextField one of the 
         * cellWidgets or the Matrix itself
         */
        mw = xbaeEventToMatrixWidget(w, event);
        if (!mw) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "traversePrevACT", "badWidget",
                                "XbaeMatrix",
                                "XbaeMatrix: Bad widget passed to TraversePrev action", NULL, 0);
                return;
        }

        XmProcessTraversal(TextField(mw), XmTRAVERSE_PREV_TAB_GROUP);
}

/**************************************************************************************************/

/* gets called on mouse events */
static void handleMotionEvent(Widget w, XtPointer data, XEvent * event, Boolean * cont)
{
        XbaeMatrixPositionStruct *pos = (XbaeMatrixPositionStruct *) data;

        if (pos->timerID) {
                XtRemoveTimeOut(pos->timerID);
                pos->timerID = 0;
        }

        if (event->type == ButtonRelease) {
                DEBUGOUT(_XbaeDebug(__FILE__, w, "handleMotionEvent ButtonRelease\n"));
                pos->grabbed = False;
        } else if (event->type == MotionNotify) {
                DEBUGOUT(_XbaeDebug(__FILE__, w, "handleMotionEvent MotionNotify\n"));
                
                pos->pointer_x = event->xmotion.x;
                pos->pointer_y = event->xmotion.y;

                pos->fcn(data);
        }
}

void waitForButtonRelease(XbaeMatrixWidget mw, Cursor cursor, XbaeMatrixPositionStruct *pos, void (*fcn)(XtPointer)) {

        unsigned long event_mask = ButtonReleaseMask | PointerMotionMask;
        Widget w = (Widget) mw;

        pos->mw = mw;
        pos->grabbed = True;
        pos->timerID = 0;
        pos->app_context = XtWidgetToApplicationContext(w);
        pos->fcn = fcn;

        XtAddEventHandler(w, event_mask, True, handleMotionEvent, (XtPointer) pos);

        XGrabPointer(XtDisplay(w), XtWindow(w), False, event_mask, GrabModeAsync, GrabModeAsync,
                     None, cursor, CurrentTime);

        while (pos->grabbed)
                XtAppProcessEvent(pos->app_context, XtIMAll);

        XUngrabPointer(XtDisplay(w), CurrentTime);

        XtRemoveEventHandler(w, event_mask, True, handleMotionEvent, (XtPointer) pos);
}

/**************************************************************************************************/

#ifdef DRAW_RESIZE_LINE
#define RESIZE_SIZE 0
#define DRAW_RESIZE(mw, display, win, dbg, x, y, w, h) \
        XDrawLine(display, win, mw->matrix.draw_gc, x, y, x + w, y + h)
#endif

#ifdef DRAW_RESIZE_SHADOW
/* These values derived through that age-old process of what looks good to me */
#define RESIZE_SIZE 4
#define DRAW_RESIZE(mw, display, win, dbg, x, y, w, h) \
        DRAW_SHADOW(display, win, dbg, \
                    mw->matrix.resize_top_shadow_gc, \
                    mw->matrix.resize_bottom_shadow_gc, RESIZE_SIZE / 2, x, y, w, h, XmSHADOW_OUT)
#endif

static void DrawSlideColumn(XbaeMatrixWidget mw, int x)
{
        Display *display = XtDisplay(mw);

        /*
         * Draw On the matrix
         */

        DRAW_RESIZE(mw, display, XtWindow(mw), "win", 
                    x, HORIZ_SB_OFFSET(mw), 
                    RESIZE_SIZE, 
                    VISIBLE_NON_FIXED_HEIGHT(mw) + VISIBLE_FIXED_ROW_HEIGHT(mw) 
                    + VISIBLE_TRAILING_FIXED_ROW_HEIGHT(mw));

        /* 
         * Draw on the clips
         */

        if (x >= FIXED_COLUMN_POSITION(mw) 
            && x < FIXED_COLUMN_POSITION(mw) + VISIBLE_FIXED_COLUMN_WIDTH(mw)) {
                if (XtIsManaged(LeftClip(mw))) {
                        DRAW_RESIZE(mw, display, XtWindow(LeftClip(mw)), "LeftClip",
                                    x - FIXED_COLUMN_POSITION(mw),
                                    0, 
                                    RESIZE_SIZE, 
                                    LeftClip(mw)->core.height);
                }
        } else if (x >= TRAILING_FIXED_COLUMN_POSITION(mw)
                   && x < TRAILING_FIXED_COLUMN_POSITION(mw) + VISIBLE_TRAILING_FIXED_COLUMN_WIDTH(mw)) {
                if (XtIsManaged(RightClip(mw))) {
                         DRAW_RESIZE(mw, display, XtWindow(RightClip(mw)), "RightClip",
                                    x - TRAILING_FIXED_COLUMN_POSITION(mw),
                                    0, 
                                    RESIZE_SIZE,
                                    RightClip(mw)->core.height);
                }
        } else if (x >= NON_FIXED_COLUMN_POSITION(mw)
                  && x < NON_FIXED_COLUMN_POSITION(mw) + VISIBLE_NON_FIXED_WIDTH(mw)) {
                if (XtIsManaged(CenterClip(mw))) {
                         DRAW_RESIZE(mw, display, XtWindow(CenterClip(mw)), "CenterClip",
                                    x - NON_FIXED_COLUMN_POSITION(mw),
                                    0, 
                                    RESIZE_SIZE,
                                    CenterClip(mw)->core.height);
                }
                if (XtIsManaged(TopClip(mw))) {
                         DRAW_RESIZE(mw, display, XtWindow(TopClip(mw)), "TopClip",
                                    x - NON_FIXED_COLUMN_POSITION(mw),
                                    0, 
                                    RESIZE_SIZE,
                                    TopClip(mw)->core.height);
                }
                if (XtIsManaged(BottomClip(mw))) {
                         DRAW_RESIZE(mw, display, XtWindow(BottomClip(mw)), "BottomClip",
                                    x - NON_FIXED_COLUMN_POSITION(mw),
                                    0, 
                                    RESIZE_SIZE,
                                    BottomClip(mw)->core.height);
                }
                if (XtIsManaged(ColumnLabelClip(mw))) {
                         DRAW_RESIZE(mw, display, XtWindow(ColumnLabelClip(mw)), "ColumnLabelClip",
                                    x - NON_FIXED_COLUMN_POSITION(mw),
                                    0, 
                                    RESIZE_SIZE,
                                    ColumnLabelClip(mw)->core.height);
                }
                
        }
}

/* Derived from DrawSlideColumn */
static void DrawSlideRow(XbaeMatrixWidget mw, int y)
{
        Display *display = XtDisplay(mw);

        /*
         * Draw On the matrix
         */

        DRAW_RESIZE(mw, display, XtWindow(mw), "win", 
                    VERT_SB_OFFSET(mw), y, 
                    VISIBLE_NON_FIXED_WIDTH(mw) + VISIBLE_FIXED_COLUMN_WIDTH(mw) 
                    + VISIBLE_TRAILING_FIXED_COLUMN_WIDTH(mw),
                    RESIZE_SIZE);

        /* 
         * Draw on the clips
         */
        
        if (y >= FIXED_ROW_POSITION(mw) 
            && y < FIXED_ROW_POSITION(mw) + VISIBLE_FIXED_ROW_HEIGHT(mw)) {
                if (XtIsManaged(TopClip(mw))) {
                        DRAW_RESIZE(mw, display, XtWindow(TopClip(mw)), "TopClip",
                                    0,
                                    y - FIXED_ROW_POSITION(mw),
                                    TopClip(mw)->core.width,
                                    RESIZE_SIZE);
                }
        } else if (y >= TRAILING_FIXED_ROW_POSITION(mw)
                   && y < TRAILING_FIXED_ROW_POSITION(mw) + VISIBLE_TRAILING_FIXED_ROW_HEIGHT(mw)) {
                if (XtIsManaged(BottomClip(mw))) {
                         DRAW_RESIZE(mw, display, XtWindow(BottomClip(mw)), "BottomClip",
                                    0, 
                                    y - TRAILING_FIXED_ROW_POSITION(mw),
                                    BottomClip(mw)->core.width,
                                    RESIZE_SIZE);
                }
        } else if (y >= NON_FIXED_ROW_POSITION(mw)
                  && y < NON_FIXED_ROW_POSITION(mw) + VISIBLE_NON_FIXED_HEIGHT(mw)) {
                if (XtIsManaged(CenterClip(mw))) {
                         DRAW_RESIZE(mw, display, XtWindow(CenterClip(mw)), "CenterClip",
                                    0, 
                                    y - NON_FIXED_ROW_POSITION(mw),
                                    CenterClip(mw)->core.width,
                                    RESIZE_SIZE);

                }
                if (XtIsManaged(LeftClip(mw))) {
                         DRAW_RESIZE(mw, display, XtWindow(LeftClip(mw)), "LeftClip",
                                    0, 
                                    y - NON_FIXED_ROW_POSITION(mw),
                                    LeftClip(mw)->core.width,
                                    RESIZE_SIZE);
                }
                if (XtIsManaged(RightClip(mw))) {
                         DRAW_RESIZE(mw, display, XtWindow(RightClip(mw)), "RightClip",
                                    0, 
                                    y - NON_FIXED_ROW_POSITION(mw),
                                    RightClip(mw)->core.width,
                                    RESIZE_SIZE);
                }
                if (XtIsManaged(RowLabelClip(mw))) {
                         DRAW_RESIZE(mw, display, XtWindow(RowLabelClip(mw)), "RowLabelClip",
                                    0, 
                                    y - NON_FIXED_ROW_POSITION(mw),
                                    RowLabelClip(mw)->core.width,
                                    RESIZE_SIZE);
                }
        }
}

static void MoveSlide(XbaeMatrixWidget mw, int event_pos, int *current_pos, short *current_size, 
        Boolean size_in_pixels, int font_size, int smallest_allowed,
        void (*DrawSlide)(XbaeMatrixWidget, int))
{
        int new_size, new_pos, delta;

        /*
         * Calculate the new size and the new position of the slider
         */
        if (size_in_pixels) {
                delta = event_pos - *current_pos;
                if (*current_size + delta < smallest_allowed) {
                    /* Must keep the size greate than smallest_allowed */
                    delta = smallest_allowed - *current_size;
                }

                new_size = *current_size + delta;
                new_pos = *current_pos + delta;
        } else {
                delta = (event_pos - *current_pos) / font_size;
                if (*current_size + delta < 1) {
                    /* Must keep the size at least on character big */
                    delta = 1 - *current_size;
                }

                new_size = *current_size + delta;
                new_pos = *current_pos + delta * font_size;
        }

        /*
         * If the new size is different from the old one, move the slider and update its position
         */
        if (new_size != *current_size) {
                /* erase the old marker */
                DrawSlide(mw, *current_pos);

                /* Save the new size of the row and the position of the marker */
                *current_size = new_size;
                *current_pos = new_pos;

                /* Draw the marker line in the new location */
                DrawSlide(mw, *current_pos);
        }
}

static void slideResize(XtPointer data)
{
        XbaeMatrixResizeStruct *rd = (XbaeMatrixResizeStruct *) data;
        XbaeMatrixWidget mw = rd->pos.mw;

        /* 
         * SGO: implemented a limit so that cells can't be smaller than 1 character
         */
        int ROW_MINIMUM_HEIGHT = (mw->matrix.row_height_in_pixels) 
                                 ? 2 * CELL_BORDER_HEIGHT(mw) + TEXT_HEIGHT(mw)
                                 : 1;
        int COLUMN_MINIMUM_WIDTH = (mw->matrix.column_width_in_pixels) 
                                   ? 2 * CELL_BORDER_WIDTH(mw)  + CELL_FONT_WIDTH(mw)
                                   : 1;

        if (rd->resize_row) {
                MoveSlide(mw, rd->pos.pointer_y, &rd->currenty, &rd->rowHeights[rd->row], 
                          mw->matrix.row_height_in_pixels, TEXT_HEIGHT(mw), ROW_MINIMUM_HEIGHT,
                          DrawSlideRow);

        }

        if (rd->resize_column) {
                MoveSlide(mw, rd->pos.pointer_x, &rd->currentx, &rd->columnWidths[rd->column], 
                          mw->matrix.column_width_in_pixels, CELL_FONT_WIDTH(mw), COLUMN_MINIMUM_WIDTH,
                          DrawSlideColumn);
        }
}

/* ARGSUSED */
void xbaeResizeColumnsACT(Widget w, XEvent * event, String * params, Cardinal * nparams)
{
        XbaeMatrixWidget mw;
        int x, y;
        int start_row, start_column;
        int i;
        XbaeMatrixResizeStruct resizeData;
        #ifdef DRAW_RESIZE_LINE
        XGCValues save, values;
        unsigned long gcmask;
        #endif
        Display *display = XtDisplay(w);
        int fuzzy = XBAE_RESIZE_FUZZ;

        /*
         * Get the Matrix widget. w could be the TextField one of the 
         * cellWidgets or the Matrix itself
         */
        mw = xbaeEventToMatrixWidget(w, event);
        if (!mw) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "resizeColumnsACT", "badWidget",
                                "XbaeMatrix",
                                "XbaeMatrix: Bad widget passed to ResizeColumns action", NULL, 0);
                return;
        }

        if (!xbaeEventToRowColumn(w, event, &start_row, &start_column, &x, &y)) {
                return;
        }

        DEBUGOUT(_XbaeDebug
                 (__FILE__, (Widget) mw, "xbaeResizeColumnsACT: x %d y %d, row %d col %d\n", x, y,
                  start_row, start_column));

        /*
         * Figure out what the fuzzyness is
         */
        if (mw->matrix.cell_shadow_thickness > fuzzy) {
                fuzzy = mw->matrix.cell_shadow_thickness;
        }
        
        /*
         * Figure out if we're resizing a row and/or a column
         */
        resizeData.resize_row = False;
        if (mw->matrix.allow_row_resize) {
                if (y <= fuzzy) {
                        /* 
                         * The click is at the top edge of the row: resize the first non hidden 
                         * row above
                         */
                        for(resizeData.row = start_row - 1;
                            resizeData.row >= 0 && mw->matrix.row_heights[resizeData.row] == 0;
                            resizeData.row--)
                                ;
                        resizeData.resize_row = (resizeData.row >= 0); /* Can't resize the column labels */
                } else if (ROW_HEIGHT(mw, start_row) - y <= fuzzy) {
                        /* 
                         * The click is at the bottom edge of a row
                         */
                        resizeData.row = start_row;
                        resizeData.resize_row = (resizeData.row >= 0); /* Can't resize the column labels */
                }
        }

        resizeData.resize_column = False;
        if (mw->matrix.allow_column_resize) {
                if (x <= fuzzy) {
                        /* 
                         * The click is at the left edge of the column: resize the first non hidden 
                         * column to the left
                         */
                        for(resizeData.column = start_column - 1;
                            resizeData.column >= 0 && mw->matrix.column_widths[resizeData.column] == 0;
                            resizeData.column--)
                                ;
                        resizeData.resize_column = (resizeData.column >= 0); /* Can't resize the row labels */
                } else if (COLUMN_WIDTH(mw, start_column) - x <= fuzzy) {
                        /* 
                         * The click is at the right edge of a column
                         */
                        resizeData.column = start_column;
                        resizeData.resize_column = (resizeData.column >= 0); /* Can't resize the row labels */
                }
        }

        if (resizeData.resize_row || resizeData.resize_column) {

                /*
                 * Make it here and it's time to start the fun stuff!
                 */

                Boolean haveText, haveHSB, haveVSB, resized_rows, resized_columns;

                haveText = mw->matrix.text_field_is_mapped;
                if (haveText) {
                        int current_row, current_column;
                        XtVaGetValues(TextField(mw), XmNattachRow, &current_row, XmNattachColumn, &current_column, NULL);
                        /* 
                         * Say goodbye to the TextField -> it only gets in the way!
                         */
                        xbaeHideTextField(mw);
                        /*
                         * Redraw the cell that had the text field in it or it might stay blank
                         */
                        xbaeDrawCell(mw, current_row, current_column);
                }

                #ifdef DRAW_RESIZE_LINE
                /*
                 * Flush the commit events out to the server.  Otherwise, our changes
                 * to the GCs below have a bad effect.
                 */
                XSync(display, False);

                gcmask = GCForeground | GCBackground | GCFunction | GCPlaneMask | GCLineWidth;
                XGetGCValues(display, mw->matrix.draw_gc, gcmask, &save);
                values.foreground = mw->core.background_pixel;
                values.background = mw->manager.foreground;
                values.function = GXxor;
                values.plane_mask = AllPlanes;
                values.line_width = 3;
                XChangeGC(display, mw->matrix.draw_gc, gcmask, &values);
                #endif

                haveHSB = False;
                if (resizeData.resize_row) {
                        if (XtIsManaged(HorizScrollChild(mw)) && !SCROLLBAR_LEFT(mw)) {
                                 /*
                                  * Say goodbye to the Horizontal ScrollBar -> it only gets in the way!
                                  */
                                 haveHSB = True;
                                 XtUnmanageChild(HorizScrollChild(mw));
                        }

                        /* Copy the rowHeight array */
                        resizeData.rowHeights = (short *) XtMalloc(mw->matrix.rows * sizeof(short));
                        for (i = 0; i < mw->matrix.rows; i++)
                                resizeData.rowHeights[i] = mw->matrix.row_heights[i];

                        DrawSlideRow(mw, event->xbutton.y);
                }

                haveVSB = False;
                if (resizeData.resize_column) {
                        if (XtIsManaged(VertScrollChild(mw)) && !SCROLLBAR_TOP(mw)) {
                                /*
                                * Say goodbye to the Vertical ScrollBar -> it only gets in the way!
                                */
                                haveVSB = True;
                                XtUnmanageChild(VertScrollChild(mw));
                        }

                        /* Copy the columnWidth array */
                        resizeData.columnWidths = (short *) XtMalloc(mw->matrix.columns * sizeof(short));
                        for (i = 0; i < mw->matrix.columns; i++)
                                resizeData.columnWidths[i] = mw->matrix.column_widths[i];

                        DrawSlideColumn(mw, event->xbutton.x);
                }

                /* Create the cursor */
                if (mw->matrix.cursor) {
                        XFreeCursor(display, mw->matrix.cursor);
                }

                if (resizeData.resize_row && resizeData.resize_column) {
                        mw->matrix.cursor = XCreateFontCursor(display, XC_sizing);
                } else if (resizeData.resize_row) {
                        mw->matrix.cursor = XCreateFontCursor(display, XC_sb_v_double_arrow);
                } else if (resizeData.resize_column) {
                        mw->matrix.cursor = XCreateFontCursor(display, XC_sb_h_double_arrow);
                }
                
                resizeData.currentx = event->xbutton.x;
                resizeData.currenty = event->xbutton.y;

                waitForButtonRelease(mw, mw->matrix.cursor, &resizeData.pos, slideResize);

                resized_rows = False;
                if (resizeData.resize_row) {
                        /*
                         * Erase the slider
                         */
                        DrawSlideRow(mw, resizeData.currenty);
                        
                        if (mw->matrix.resize_row_callback) {
                                XbaeMatrixResizeRowCallbackStruct call_data;

                                call_data.reason = XbaeResizeRowReason;
                                call_data.event = event;
                                call_data.row = start_row;
                                call_data.column = start_column;
                                call_data.which = resizeData.row;
                                call_data.rows = mw->matrix.rows;
                                call_data.row_heights = resizeData.rowHeights;
                                XtCallCallbackList((Widget) mw, mw->matrix.resize_row_callback,
                                                   (XtPointer) & call_data);
                        }

                        for (i = 0; !resized_rows && i < mw->matrix.rows; i++) {
                                resized_rows = (resizeData.rowHeights[i] != mw->matrix.row_heights[i]);
                        }
                }

                resized_columns = False;
                if (resizeData.resize_column) {
                        /*
                         * Erase the slider
                         */
                        DrawSlideColumn(mw, resizeData.currentx);

                        if (mw->matrix.resize_column_callback) {
                                XbaeMatrixResizeColumnCallbackStruct call_data;

                                call_data.reason = XbaeResizeColumnReason;
                                call_data.event = event;
                                call_data.row = start_row;
                                call_data.column = start_column;
                                call_data.which = resizeData.column;
                                call_data.columns = mw->matrix.columns;
                                call_data.column_widths = resizeData.columnWidths;
                                XtCallCallbackList((Widget) mw, mw->matrix.resize_column_callback,
                                                   (XtPointer) & call_data);
                        }

                        for (i = 0; !resized_columns && i < mw->matrix.columns; i++) {
                                resized_columns = (resizeData.columnWidths[i] != mw->matrix.column_widths[i]);
                        }
                }

                #ifdef DRAW_RESIZE_LINE
                XChangeGC(display, mw->matrix.draw_gc, gcmask, &save);
                #endif

                /*
                 * Make sure everything is handled correctly with SetValues 
                 */
                if (resized_rows && resized_columns) {
                        XtVaSetValues((Widget) mw, 
                                      XmNrowHeights, resizeData.rowHeights,
                                      XmNcolumnWidths, resizeData.columnWidths, 
                                      NULL);
                } else if (resized_rows) {
                        XtVaSetValues((Widget) mw, 
                                      XmNrowHeights, resizeData.rowHeights,
                                      NULL);
                } else if (resized_columns) {
                        XtVaSetValues((Widget) mw, 
                                      XmNcolumnWidths, resizeData.columnWidths, 
                                      NULL);
                } else {
                        /* 
                         * Since we don't call SetValues in this case, we must remanage the 
                         * scrollbars if we unmanaged them.
                         */

                        if (haveHSB) {
                                XtManageChild(HorizScrollChild(mw));
                        }

                        if (haveVSB) {
                                XtManageChild(VertScrollChild(mw));
                        }
                }
                
                if (resizeData.resize_row) {
                        XtFree((XtPointer) resizeData.rowHeights);
                }
                if (resizeData.resize_column) {
                        XtFree((XtPointer) resizeData.columnWidths);
                }

                if (haveText) {
                        xbaePositionTextField(mw);
                }
        }
}

/**************************************************************************************************/

 /*ARGSUSED*/ 
static void scrollLabel(XtPointer data)
{
        XbaeMatrixButtonPressedStruct *buttonData = (XbaeMatrixButtonPressedStruct *) data;
        XbaeMatrixPositionStruct *pos = &buttonData->pos;
        XbaeMatrixWidget mw = pos->mw;
        Boolean scrolling = False;
        Boolean pressed;
        int row, column;
        int x = pos->pointer_x;
        int y = pos->pointer_y;
        
        if (!pos->grabbed)
                return;

        if (!pos->timerID) {
                /* 
                 * We are called following a pointer motion. Recalculate the distance
                 * the pointer is out of the scrollable region
                 */
                buttonData->disty = 0;
                if (buttonData->initial_region & CLIP_VISIBLE_HEIGHT) {
                        if (y < NON_FIXED_ROW_POSITION(mw)) {
                            buttonData->disty = y - NON_FIXED_ROW_POSITION(mw);
                        } else if (y > NON_FIXED_ROW_POSITION(mw) + VISIBLE_NON_FIXED_HEIGHT(mw) - 1) {
                            buttonData->disty = y - (NON_FIXED_ROW_POSITION(mw) + VISIBLE_NON_FIXED_HEIGHT(mw) - 1);
                        }
                }

                buttonData->distx = 0;
                if (buttonData->initial_region & CLIP_VISIBLE_WIDTH) {
                        if (x < NON_FIXED_COLUMN_POSITION(mw)) {
                            buttonData->distx = x - NON_FIXED_COLUMN_POSITION(mw);
                        } else if (x > NON_FIXED_COLUMN_POSITION(mw) + VISIBLE_NON_FIXED_WIDTH(mw) - 1) {
                            buttonData->distx = x - (NON_FIXED_COLUMN_POSITION(mw) + VISIBLE_NON_FIXED_WIDTH(mw) - 1);
                        }
                }
        }
        
        /*
         * If we are off the originial clip widget and there are cells that could
         * be scrolled into view do so. 
         */
        if (buttonData->initial_region & CLIP_VISIBLE_HEIGHT) {
                if (buttonData->disty < 0) {
                        xbaeScrollRows(mw, buttonData->disty);
                        scrolling = True;
                } else if (buttonData->disty > 0) {
                        xbaeScrollRows(mw, buttonData->disty);
                        scrolling = True;
                }
        }
        if (buttonData->initial_region & CLIP_VISIBLE_WIDTH) {
                if (buttonData->distx < 0) {
                        xbaeScrollColumns(mw, buttonData->distx);
                        scrolling = True;
                } else if (buttonData->distx > 0) {
                        xbaeScrollColumns(mw, buttonData->distx);
                        scrolling = True;
                }
        }
        
        /*
         * If the status of whether or not the button should be pressed has
         * changed, redraw the appropriate visual 
         */

        xbaeMatrixXYToRowCol(mw, &x, &y, &row, &column);

        pressed = (row == buttonData->label_row && column == buttonData->label_column);

        if (pressed != buttonData->pressed) {
                if (buttonData->label_column == -1)
                        xbaeDrawRowLabel(mw, buttonData->label_row, pressed);
                else if (buttonData->label_row == -1)
                        xbaeDrawColumnLabel(mw, buttonData->label_column, pressed);
                /* And set our struct's pressed member to the current setting */
                buttonData->pressed = pressed;
        }

        if (scrolling) {
                /*
                 * Flush the updates out to the server so we don't end up 
                 * lagging behind too far and end up with a million redraw 
                 * requests. Particularly for higher update speeds.
                 */
                XFlush(XtDisplay((Widget) mw));
                /*
                 * Reschedule this function so it can keep scrolling
                 */
                pos->timerID =
                    XtAppAddTimeOut(pos->app_context, DEFAULT_SCROLL_SPEED, 
                                    (XtTimerCallbackProc) scrollLabel, data);
        } else {
                pos->timerID = 0;
        }
}

void xbaeLabelACT(Widget w, XEvent * event, String * params, Cardinal * nparams)
{
        XbaeMatrixWidget mw;
        XbaeMatrixButtonPressedStruct buttonData;
        int x, y;
        int row, column, initial_region;

        DEBUGOUT(_XbaeDebug(__FILE__, w, "xbaeLabelACT()\n"));

        /*
         * Get the Matrix widget. w could be the TextField one of the 
         * cellWidgets or the Matrix itself
         */
        mw = xbaeEventToMatrixWidget(w, event);
        if (!mw) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "xbaeLabelACT", "badWidget",
                                "XbaeMatrix",
                                "XbaeMatrix: Bad widget passed to Label action", NULL, 0);
                return;
        }

        /*
         * We are only interested in ButtonPress events - the ButtonRelease
         * events are handled in the event handler loop below.
         */
        if (event->type != ButtonPress) {
                return;
        }

        initial_region = xbaeEventToRowColumn(w, event, &row, &column, &x, &y);
        
        if (   (mw->matrix.button_labels 
                && (initial_region & (CLIP_ROW_LABELS | CLIP_COLUMN_LABELS)))
            || (mw->matrix.column_button_labels && mw->matrix.column_button_labels[column]
                && (initial_region & CLIP_COLUMN_LABELS))
            || (mw->matrix.row_button_labels && mw->matrix.row_button_labels[row]
                && (initial_region & CLIP_COLUMN_LABELS))) {

                if (column == -1) {
                        /* row label */
                        DEBUGOUT(_XbaeDebug(__FILE__, w, "Action in row label\n"));
                        xbaeDrawRowLabel(mw, row, True);
                } else if (row == -1) {
                        /* Column label */
                        DEBUGOUT(_XbaeDebug(__FILE__, w, "Action in column label\n"));
                        xbaeDrawColumnLabel(mw, column, True);
                }

                /* Action stations! */

                buttonData.label_row = row;
                buttonData.label_column = column;
                buttonData.pressed = True;

                buttonData.initial_region = initial_region;
                buttonData.distx = 0;
                buttonData.disty = 0;

                waitForButtonRelease(mw, None, &buttonData.pos, scrollLabel);

                if (buttonData.pressed) {
                        /* 
                         * If the button is still pressed, it has been released in the
                         * same button that was pressed.  "Unpress" it and call the
                         * callbacks 
                         */
                        if (column == -1) {
                                xbaeDrawRowLabel(mw, row, False);
                        } else if (row == -1) {
                                xbaeDrawColumnLabel(mw, column, False);
                        }

                        if (mw->matrix.label_activate_callback) {
                                XbaeMatrixLabelActivateCallbackStruct call_data;

                                call_data.reason = XbaeLabelActivateReason;
                                call_data.event = event;
                                call_data.row_label = (column == -1);
                                call_data.row = row;
                                call_data.column = column;

                                if (column == -1)
                                        call_data.label =
                                            mw->matrix.row_labels[row];
                                else
                                        call_data.label =
                                            mw->matrix.column_labels[column];

                                XtCallCallbackList((Widget) mw,
                                                   mw->matrix.label_activate_callback,
                                                   (XtPointer) & call_data);
                        }
                }
        }
}

/**************************************************************************************************/

static void
callSelectCellCallbacks(XbaeMatrixWidget mw, XEvent * event, int row, int column, String * params,
                        Cardinal num_params)
{
        XbaeMatrixSelectCellCallbackStruct call_data;

        call_data.reason = XbaeSelectCellReason;
        call_data.event = event;
        call_data.row = row;
        call_data.column = column;
        call_data.num_params = num_params;
        call_data.params = params;
        call_data.cells = NULL; /* mw->matrix.cells */
        call_data.selected_cells = NULL;        /* mw->matrix.selected_cells */
        /*
         * Need to create a new structure and pass this to the callback.
         * Before the PER_CELL change, this was an easy action because
         * we could pass an existing structure. Now we need to build
         * it especially for this callback.
         */

        XtCallCallbackList((Widget) mw, mw->matrix.select_cell_callback, (XtPointer) & call_data);
}

static void scrollSelect(XtPointer data)
{
        XbaeMatrixScrollStruct *ss = (XbaeMatrixScrollStruct *) data;
        XbaeMatrixPositionStruct *pos = &ss->pos;
        XbaeMatrixWidget mw = pos->mw;
        Boolean scrolling = False;
        int row, column, next_row, next_column, current_row_region, current_column_region;
        int x = pos->pointer_x;
        int y = pos->pointer_y;

        if (!pos->grabbed)
                return;

        if (!pos->timerID) {
                /* 
                 * We are called following a pointer motion. Recalculate the distance
                 * the pointer is out of the scrollable region
                 */
                ss->disty = 0;
                if (ss->initial_region & CLIP_VISIBLE_HEIGHT) {
                        if (y < NON_FIXED_ROW_POSITION(mw)) {
                            ss->disty = y - NON_FIXED_ROW_POSITION(mw);
                        } else if (y > NON_FIXED_ROW_POSITION(mw) + VISIBLE_NON_FIXED_HEIGHT(mw) - 1) {
                            ss->disty = y - (NON_FIXED_ROW_POSITION(mw) + VISIBLE_NON_FIXED_HEIGHT(mw) - 1);
                        }
                }

                ss->distx = 0;
                if (ss->initial_region & CLIP_VISIBLE_WIDTH) {
                        if (x < NON_FIXED_COLUMN_POSITION(mw)) {
                            ss->distx = x - NON_FIXED_COLUMN_POSITION(mw);
                        } else if (x > NON_FIXED_COLUMN_POSITION(mw) + VISIBLE_NON_FIXED_WIDTH(mw) - 1) {
                            ss->distx = x - (NON_FIXED_COLUMN_POSITION(mw) + VISIBLE_NON_FIXED_WIDTH(mw) - 1);
                        }
                }
        }

        /*
         * If we are off the originial clip widget and there are cells that could
         * be scrolled into view do so. 
         */
        next_row = ss->last_row;
        if (ss->initial_region & CLIP_VISIBLE_HEIGHT) {
                if (ss->disty < 0) {
                        xbaeScrollRows(mw, ss->disty);
                        next_row = xbaeTopRow(mw);
                        scrolling = True;
                } else if (ss->disty > 0) {
                        xbaeScrollRows(mw, ss->disty);
                        next_row = xbaeBottomRow(mw);
                        scrolling = True;
                }
        }
        
        next_column = ss->last_column;
        if (ss->initial_region & CLIP_VISIBLE_WIDTH) {
                if (ss->distx < 0) {
                        xbaeScrollColumns(mw, ss->distx);
                        next_column = xbaeLeftColumn(mw);
                        scrolling = True;
                } else if (ss->distx > 0) {
                        xbaeScrollColumns(mw, ss->distx);
                        next_column = xbaeRightColumn(mw);
                        scrolling = True;
                }
        }

        /*
         * Figure out what cell to select next
         */
        current_row_region = xbaeMatrixYtoRow(mw, &y, &row);
        current_column_region = xbaeMatrixXtoColumn(mw, &x, &column);

        if (current_row_region & ss->initial_region) {
                next_row = row;
        }
        
        if (current_column_region & ss->initial_region) {
                next_column = column;
        }
        
        /*
         * If we are not over the same cell as the last time arround, 
         * call the selectCellCB
         */
        if ((next_row != ss->last_row || next_column != ss->last_column)
            && (   mw->matrix.selection_policy == XmMULTIPLE_SELECT
                || mw->matrix.selection_policy == XmEXTENDED_SELECT)) {
                //Boolean old_scroll_select = mw->matrix.scroll_select;
                //mw->matrix.scroll_select = False;

                callSelectCellCallbacks(mw, ss->event, next_row, next_column, ss->params, ss->num_params);

                //mw->matrix.scroll_select = old_scroll_select;

                ss->last_row = next_row;
                ss->last_column = next_column;
        }

        if (scrolling) {
                /*
                 * Flush the updates out to the server so we don't end up 
                 * lagging behind too far and end up with a million redraw 
                 * requests. Particularly for higher update speeds.
                 */
                XFlush(XtDisplay((Widget) mw));
                /*
                 * Reschedule this function so it can keep scrolling
                 */
                pos->timerID =
                    XtAppAddTimeOut(pos->app_context, DEFAULT_SCROLL_SPEED, 
                                    (XtTimerCallbackProc) scrollSelect, data);
        } else {
                pos->timerID = 0;
        }
}

/* ARGSUSED */
void xbaeSelectCellACT(Widget w, XEvent * event, String * params, Cardinal * num_params)
{
        XbaeMatrixWidget mw;
        int x, y;
        int row, column, initial_region;
        XbaeMatrixScrollStruct scrollData;

        /*
         * Get the Matrix widget. w could be the TextField one of the 
         * cellWidgets or the Matrix itself
         */
        mw = xbaeEventToMatrixWidget(w, event);
        if (!mw) {
                XtAppWarningMsg(XtWidgetToApplicationContext(w), "xbaeSelectCellACT", "badWidget",
                                "XbaeMatrix", "XbaeMatrix: Bad widget passed to SelectCell action",
                                NULL, 0);
                return;
        }
        
        initial_region = xbaeEventToRowColumn(w, event, &row, &column, &x, &y);
        if (initial_region == 0) {
                return;
        }
        
        /*
         * Call our select_cell callbacks
         */
        if (mw->matrix.select_cell_callback) {
                callSelectCellCallbacks(mw, event, row, column, params, *num_params);
        }
        
        /*
         * When the click was in a label don't scroll select
         */
        if (initial_region & (CLIP_ROW_LABELS | CLIP_COLUMN_LABELS)) {
                return;
        }

        /*
         * If it's not a buttonPress event, don't start scrolling
         */
        if (event->type != ButtonPress) {
                return;
        }
        
        /*
         * If the first parameter isn't "Pointer", don't scroll select 
         */
        if (*num_params == 0 || strcmp(params[0], "PointerExtend") != 0) {
                return;
        }

        scrollData.event = event;
        scrollData.num_params = *num_params;
        scrollData.params = params;
        scrollData.initial_region = initial_region;

        scrollData.last_row = row;
        scrollData.last_column = column;
        scrollData.distx = 0;
        scrollData.disty = 0;
        
        params[0] += sizeof("Pointer") - 1;
        waitForButtonRelease(mw, None, &scrollData.pos, scrollSelect);
        params[0] -= sizeof("Pointer") - 1;
}

void xbaeHandleMotionACT(Widget w, XEvent * event, String * params, Cardinal * nparams){
       /* The work here is now done in selectCellAction. This function
        * is here only so people don't get warnings about missing actions
        */
}

