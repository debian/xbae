/*
 * Copyright(c) 1992 Bell Communications Research, Inc. (Bellcore)
 *                        All rights reserved
 *
 * Copyright � 1999, 2001 by the LessTif Developers.
 *
 * Permission to use, copy, modify and distribute this material for
 * any purpose and without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies, and that the name of Bellcore not be used in advertising
 * or publicity pertaining to this material without the specific,
 * prior written permission of an authorized representative of
 * Bellcore.
 *
 * BELLCORE MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES, EX-
 * PRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST IN-
 * FRINGEMENT OF PATENTS OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE
 * SOFTWARE IS PROVIDED "AS IS", AND IN NO EVENT SHALL BELLCORE OR
 * ANY OF ITS AFFILIATES BE LIABLE FOR ANY DAMAGES, INCLUDING ANY
 * LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES RELAT-
 * ING TO THE SOFTWARE.
 *
 * $Id: caption.c,v 1.7 2001/03/30 07:52:02 dannybackx Exp $
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif

#include <stdlib.h>
#ifdef USE_EDITRES
#include <X11/Intrinsic.h>
#include <X11/Xmu/Editres.h>
#endif

#include <Xm/DialogS.h>
#include <Xm/TextF.h>
#include <Xm/Form.h>
#include <Xm/Frame.h>
#include <Xm/RowColumn.h>
#include <Xm/PushB.h>
#include <Xm/ToggleB.h>
#include <Xbae/Caption.h>

/*
 * Some examples of Captions use
 */

void popupCaptionTextF(Widget w, String name, XtPointer call_data);
void popupCaptionFrame(Widget w, String name, XtPointer call_data);
void popupCaptionToggle(Widget w, XtPointer client_data, XtPointer call_data);

static String fallback[] = {
	"*button1.labelString:			Text, caption right",
	"*button2.labelString:			ButtonBox, caption top center",
	"*button3.labelString:			ButtonBox, multiline caption left",
	"*button4.labelString:			Text, caption left",
	"*button5.labelString:			Double toggle boxes",
	"*caption1.allowShellResize:		True",
	"*caption1*fontList:			-adobe-helvetica-*-r-*-*-*-180-*-*-*-*-*-*",
	"*caption1*text.resizeWidth:		True",
	"*caption1*cw.labelString:		Caption",
	"*caption1*cw.labelPosition:		right",
	"*caption1*cw.labelAlignment:		center",
	"*caption1*cw.spacing:			30",
	"*caption2*fontList:			-adobe-helvetica-*-r-*-*-*-180-*-*-*-*-*-*",
/* Position the caption at the to center of the Frame and offset it
 * so that it overlaps slightly. */
	"*caption2*cw.wcClassName:		XbaeCaption",
	"*caption2*cw.labelString:		Caption",
	"*caption2*cw.labelPosition:		top",
	"*caption2*cw.labelAlignment:		center",
	"*caption2*cw.labelOffset:		-15",
	"*caption2*cw.Offset:			10",
	"*caption2*cw.leftAttachment:		attach_form",
	"*caption2*cw.rightAttachment:		attach_form",
	"*caption2*cw.bottomAttachment:		attach_form",
	"*caption2*cw.topAttachment:		attach_form",
	"*caption2*frame.marginWidth:		15",
	"*caption2*frame.marginHeight:		15",
	"*caption2*frame.shadowThickness:	4",
	"*caption2*rc.orientation:		vertical",
	"*caption2*button.labelString:		PushButton",
	"*caption3*fontList:			-adobe-helvetica-*-r-*-*-*-180-*-*-*-*-*-*",
	"*caption3*cw.wcClassName:		XbaeCaption",
	"*caption3*cw.labelString:		Multi\nLine\nCaption",
	"*caption3*cw.labelPosition:		left",
	"*caption3*cw.labelAlignment:		top",
	"*caption3*cw.labelOffset:		5",
	"*caption3*cw.labelTextAlignment:	alignment_end",
	"*caption3*cw.Offset:			10",
	"*caption3*cw.leftAttachment:		attach_form",
	"*caption3*cw.rightAttachment:		attach_form",
	"*caption3*cw.bottomAttachment:		attach_form",
	"*caption3*cw.topAttachment:		attach_form",
	"*caption3*frame.marginWidth:		15",
	"*caption3*frame.marginHeight:		15",
	"*caption3*frame.shadowThickness:	4",
	"*caption3*rc.orientation:		vertical",
	"*caption3*button.labelString:		PushButton",
	"*caption4*fontList:			-adobe-helvetica-*-r-*-*-*-180-*-*-*-*-*-*",
	"*caption4*cw.labelString:		Caption:",
	"*caption4*cw.labelOffset:		5",
	"*caption4*text.columns:		10",
	"*caption5*fontList:			-adobe-helvetica-*-r-*-*-*-180-*-*-*-*-*-*",
	"*caption5*cw1.labelString:		ToggleBox 1",
	"*caption5*cw1.labelPosition:		top",
	"*caption5*cw1.labelAlignment:		left",
	"*caption5*cw1.labelOffset:		-15",
	"*caption5*cw1.Offset:			10",
	"*caption5*cw1.leftAttachment:		attach_form",
	"*caption5*cw1.rightAttachment:		attach_form",
	"*caption5*cw1.topAttachment:		attach_form",
	"*caption5*cw1.bottomAttachment:	attach_position",
	"*caption5*cw1.bottomPosition:		50",
	"*caption5*cw2.labelString:		ToggleBox 2",
	"*caption5*cw2.labelPosition:		top",
	"*caption5*cw2.labelAlignment:		left",
	"*caption5*cw2.labelOffset:		-15",
	"*caption5*cw2.Offset:			10",
	"*caption5*cw2.leftAttachment:		attach_form",
	"*caption5*cw2.rightAttachment:		attach_form",
	"*caption5*cw2.bottomAttachment:	attach_form",
	"*caption5*cw2.topAttachment:		attach_position",
	"*caption5*cw2.topPosition:		50",
	"*caption5*frame.marginWidth:		15",
	"*caption5*frame.marginHeight:		15",
	"*caption5*frame.shadowThickness:	4",
	"*caption5*button.labelString:		Toggle",
	NULL
};

int
main(int argc, char *argv[])
{
    Widget toplevel, rc, button;
    XtAppContext app;

    toplevel = XtVaAppInitialize(&app, "Caption",
				 NULL, 0,
				 &argc, argv,
				 fallback,
				 NULL);
#ifdef USE_EDITRES
    XtAddEventHandler( toplevel, (EventMask)0, True,
                       _XEditResCheckMessages, NULL);
#endif
    

    rc = XtVaCreateManagedWidget("rc",
				 xmRowColumnWidgetClass, toplevel,
				 NULL);

    button = XtVaCreateManagedWidget("button1",
				     xmPushButtonWidgetClass, rc,
				     NULL);
    XtAddCallback(button, XmNactivateCallback, (XtCallbackProc)popupCaptionTextF,
		  (XtPointer)"caption1");
    button = XtVaCreateManagedWidget("button2",
				     xmPushButtonWidgetClass, rc,
				     NULL);
    XtAddCallback(button, XmNactivateCallback, (XtCallbackProc)popupCaptionFrame,
		  (XtPointer)"caption2");
    button = XtVaCreateManagedWidget("button3",
				     xmPushButtonWidgetClass, rc,
				     NULL);
    XtAddCallback(button, XmNactivateCallback, (XtCallbackProc)popupCaptionFrame,
		  (XtPointer)"caption3");
    button = XtVaCreateManagedWidget("button4",
				     xmPushButtonWidgetClass, rc,
				     NULL);
    XtAddCallback(button, XmNactivateCallback, (XtCallbackProc)popupCaptionTextF,
		  (XtPointer)"caption4");
    button = XtVaCreateManagedWidget("button5",
				     xmPushButtonWidgetClass, rc,
				     NULL);
    XtAddCallback(button, XmNactivateCallback, (XtCallbackProc)popupCaptionToggle, NULL);
    
    XtRealizeWidget(toplevel);
    XtAppMainLoop(app);

    return 0;
}

/* ARGSUSED */
void
popupCaptionTextF(Widget w, String name, XtPointer call_data)
{
    Widget shell, cw;

    shell = XtCreatePopupShell(name,
			       xmDialogShellWidgetClass, w,
			       NULL, 0);
    
    cw = XtCreateWidget("cw",
			xbaeCaptionWidgetClass, shell,
			NULL, 0);

    XtVaCreateManagedWidget("text",
			    xmTextFieldWidgetClass, cw,
			    NULL);

    XtManageChild(cw);
}

/* ARGSUSED */
void
popupCaptionFrame(Widget w, String name, XtPointer call_data)
{
    Widget shell, form, cw, frame, rc;
    int i;

    shell = XtCreatePopupShell(name,
			       xmDialogShellWidgetClass, w,
			       NULL, 0);

    form = XtCreateWidget("form",
			  xmFormWidgetClass, shell,
			  NULL, 0);
    cw = XtVaCreateManagedWidget("cw",
				 xbaeCaptionWidgetClass, form,
				 NULL);
    frame = XtVaCreateManagedWidget("frame",
				    xmFrameWidgetClass, cw,
				    NULL);
    rc = XtVaCreateManagedWidget("rc",
				 xmRowColumnWidgetClass, frame,
				 NULL);
    for (i = 0; i < 3; i++)
	XtVaCreateManagedWidget("button",
				xmPushButtonWidgetClass, rc,
				NULL);

    XtManageChild(form);
}

/* ARGSUSED */
void
popupCaptionToggle(Widget w, XtPointer client_data, XtPointer call_data)
{
    Widget shell, form, cw, frame, rc;
    int i;

    shell = XtCreatePopupShell("caption5",
			       xmDialogShellWidgetClass, w,
			       NULL, 0);

    form = XtCreateWidget("form",
				   xmFormWidgetClass, shell,
				   NULL, 0);

    cw = XtVaCreateManagedWidget("cw1",
				 xbaeCaptionWidgetClass, form,
				 NULL);
    frame = XtVaCreateManagedWidget("frame",
				    xmFrameWidgetClass, cw,
				    NULL);
    rc = XmCreateRadioBox(frame, "rc", NULL, 0);
    XtManageChild(rc);

    for (i = 0; i < 4; i++)
	XtVaCreateManagedWidget("button",
				xmToggleButtonWidgetClass, rc,
				NULL);


    cw = XtVaCreateManagedWidget("cw2",
				 xbaeCaptionWidgetClass, form,
				 NULL);
    frame = XtVaCreateManagedWidget("frame",
				    xmFrameWidgetClass, cw,
				    NULL);
    rc = XmCreateRadioBox(frame, "rc", NULL, 0);
    XtManageChild(rc);

    for (i = 0; i < 4; i++)
	XtVaCreateManagedWidget("button",
				xmToggleButtonWidgetClass, rc,
				NULL);

    XtManageChild(form);
}
