/*
 * Copyright(c) 1992 Bell Communications Research, Inc. (Bellcore)
 *                        All rights reserved
 *
 * Copyright � 2001 by the LessTif Developers
 *
 * Permission to use, copy, modify and distribute this material for
 * any purpose and without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies, and that the name of Bellcore not be used in advertising
 * or publicity pertaining to this material without the specific,
 * prior written permission of an authorized representative of
 * Bellcore.
 *
 * BELLCORE MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES, EX-
 * PRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST IN-
 * FRINGEMENT OF PATENTS OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE
 * SOFTWARE IS PROVIDED "AS IS", AND IN NO EVENT SHALL BELLCORE OR
 * ANY OF ITS AFFILIATES BE LIABLE FOR ANY DAMAGES, INCLUDING ANY
 * LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES RELAT-
 * ING TO THE SOFTWARE.
 *
 * $Id: chooser1.c,v 1.5 2005/05/25 04:53:46 tobiasoed Exp $
 */

#include <stdlib.h>
#include <stdio.h>

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif
#include <stdlib.h>
#ifdef USE_EDITRES
#include <X11/Intrinsic.h>
#include <X11/Xmu/Editres.h>
#endif
#include <Xbae/Matrix.h>
#include <Xm/XmAll.h>

static String fallback[] = {
	"Matrix*mw.rowLabels:		1, 2, 3, 4, 5, 6",
	"Matrix*mw.columns:		5",
	"Matrix*mw.rows:		6",
	"Matrix*mw.columnWidths:	8, 3, 10, 20, 10",
	"Matrix*mw.allowColumnResize:	True",
	"Matrix*mw.cells:		Orange, 12, Rough, Inches, Large\\n"
	"				Blue, 323, Smooth, Feet, Medium\\n"
	"				Yellow, 456, Bristly, Meters, Large\\n"
	"				Green, 1, Knobby, Miles, Small\\n"
	"				Pink, 33, Hairy, Quarts, Small\\n"
	"				Black, 7, Silky, Gallons, Small",
	"Matrix*mw.cellBackgrounds:	white, antiquewhite, white, white, antiquewhite\\n"
	"				white, antiquewhite, white, white, antiquewhite\\n"
	"				white, antiquewhite, white, white, antiquewhite\\n"
	"				white, antiquewhite, white, white, antiquewhite\\n"
	"				white, antiquewhite, white, white, antiquewhite\\n"
	"				white, antiquewhite, white, white, antiquewhite",
	"Matrix*mw.colors: 		blue, black, blue, blue, black\\n"
	"				blue, black, blue, blue, black\\n"
	"				blue, black, blue, blue, black\\n"
	"				blue, black, blue, blue, black\\n"
	"				blue, black, blue, blue, black\\n"
	"				blue, black, blue, blue, black",
	"Matrix*mw.columnLabels:	Color, #, Texture, Measure, Size",
	"Matrix*mw.columnLabelAlignments:alignment_beginning, alignment_center,"
	"				alignment_beginning, alignment_beginning,"
	"				alignment_beginning",
	"Matrix*mw.columnAlignments:	alignment_beginning, alignment_end,"
	"				alignment_beginning, alignment_beginning,"
	"				alignment_beginning",
	"Matrix*labelFont:		-*-helvetica-bold-r-*-*-14-*-*-*-*-*-*-*",
	"Matrix*fontList:		-*-helvetica-medium-r-*-*-10-*-*-*-*-*-*-*",
	"Matrix*cellHighlightThickness: 2",
	"Matrix*cellShadowThickness:	2",
	"Matrix*cellMarginWidth:	0",
	"Matrix*cellMarginHeight:	3",
	"Matrix*gridType:		grid_cell_shadow",
	"Matrix*cellShadowType:		shadow_in",
	"Matrix*rowLabelColor:		Red",
	"Matrix*columnLabelColor:	Blue",
	"Matrix*mw.buttonLabels:	True",
	"Matrix*mw.allowColumnResize:	True",
	NULL
};

void
labelCB(Widget mw, XtPointer cd, XtPointer cb)
{
    XbaeMatrixLabelActivateCallbackStruct *cbs =
	( XbaeMatrixLabelActivateCallbackStruct * )cb;

    if( cbs->row_label )
	if( XbaeMatrixIsRowSelected( mw, cbs->row ) )
	    XbaeMatrixDeselectRow( mw, cbs->row );
	else
	    XbaeMatrixSelectRow( mw, cbs->row );
    else
	if( XbaeMatrixIsColumnSelected( mw, cbs->column ) )
	    XbaeMatrixDeselectColumn( mw, cbs->column );
	else
	    XbaeMatrixSelectColumn( mw, cbs->column );
}

void
CreateChooser(Widget mat, int row, int col, int n)
{
	int	i;
	Widget	option, menu, but;

	option = XmCreateOptionMenu(mat, "menu", NULL, 0);
	XtManageChild(option);
	menu = XmCreatePulldownMenu(mat, "menu", NULL, 0);
	XtVaSetValues(option, XmNsubMenuId, menu, NULL);
	for (i=0; i<n; i++) {
		but = XmCreatePushButton(menu, "button", NULL, 0);
		XtManageChild(but);
	}
	XbaeMatrixSetCellWidget(mat, row, col, option);
}

static void enter(Widget w, XtPointer client, XtPointer call)
{
	XbaeMatrixEnterCellCallbackStruct	*cbp = (XbaeMatrixEnterCellCallbackStruct *)call;
	fprintf(stderr, "Enter %d %d\n", cbp->row, cbp->column);
}

static void leave(Widget w, XtPointer client, XtPointer call)
{
	XbaeMatrixLeaveCellCallbackStruct	*cbp = (XbaeMatrixLeaveCellCallbackStruct *)call;
	fprintf(stderr, "Leave %d %d\n", cbp->row, cbp->column);
}

/*
 * Simple example of loaded Matrix
 */

int
main(int argc, char *argv[])
{
    Widget toplevel, mw, arrow, arrowg;
    XtAppContext app;


    toplevel = XtVaAppInitialize(&app, "Matrix",
				 NULL, 0,
				 &argc, argv,
				 fallback,
				 NULL);
#ifdef USE_EDITRES
    XtAddEventHandler( toplevel, (EventMask)0, True,
                       _XEditResCheckMessages, NULL);
#endif

    mw = XtVaCreateManagedWidget("mw",
				 xbaeMatrixWidgetClass, toplevel,
				 NULL);

    XtAddCallback( mw, XmNlabelActivateCallback, ( XtCallbackProc )labelCB,
		   NULL );
    XtAddCallback(mw, XmNleaveCellCallback, leave, NULL);
    XtAddCallback(mw, XmNenterCellCallback, enter, NULL);

    CreateChooser(mw, 2, 3, 5);

    arrow = XtVaCreateManagedWidget("arrow", xmArrowButtonWidgetClass, mw,
		XmNarrowDirection, XmARROW_UP,
		NULL);
    XbaeMatrixSetCellWidget(mw, 3, 4, arrow);
    
    arrowg = XtVaCreateManagedWidget("arrow", xmArrowButtonGadgetClass, mw,
		XmNarrowDirection, XmARROW_LEFT,
		NULL);
    XbaeMatrixSetCellWidget(mw, 4, 4, arrowg);
    
    XtRealizeWidget(toplevel);
    XtAppMainLoop(app);
    
    /*NOTREACHED*/
    return 0;
}

