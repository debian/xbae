/* main.C
 * by J. C. Georgas
 * 2005-02-13
 * Test program for the XbaeMatrix traversal bug.
 */

#include <stdio.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Form.h>
#include <Xm/PushB.h>
#include <Xm/RowColumn.h>
#include <Xm/Xm.h>
#include <Xbae/Matrix.h>

static void commitCallback( Widget w
		, XtPointer client_data, XtPointer call_data );

static void cancelCallback( Widget w
		, XtPointer client_data, XtPointer call_data );

static void matrixCallback( Widget w
		, XtPointer client_data, XtPointer call_data );

int main( int argc, char* argv[] )
{
    Widget toplevel, form, matrix;
    Widget buttons, commit, cancel;
	XtAppContext app;
	XtSetLanguageProc( NULL, NULL, NULL );
	toplevel = XtVaOpenApplication( &app, "XSass"
			, NULL, 0
			, &argc, argv
			, NULL
			, sessionShellWidgetClass
			, XmNdeleteResponse, XmDO_NOTHING
			, NULL );

	/* add a matrix and a couple of pushbuttons: */
    form = XtVaCreateManagedWidget( "form"
			, xmFormWidgetClass
			, toplevel
			, NULL );
    
	matrix = XtVaCreateManagedWidget( "matrix"
			, xbaeMatrixWidgetClass
			, form
			, XmNrows, 4
			, XmNcolumns, 4
			, NULL );

	XtAddCallback( matrix, XmNenterCellCallback, matrixCallback, "enter" );
	XtAddCallback( matrix, XmNleaveCellCallback, matrixCallback, "leave" );
	XtAddCallback( matrix, XmNtraverseCellCallback, matrixCallback, "traverse" );

	buttons = XtVaCreateManagedWidget( "buttons"
			, xmRowColumnWidgetClass
			, form
			, XmNleftAttachment, XmATTACH_WIDGET
			, XmNleftWidget, matrix
			, NULL );

	commit = XtVaCreateManagedWidget( "commit"
			, xmPushButtonWidgetClass
			, buttons
			, NULL );

	XtAddCallback( commit, XmNactivateCallback, commitCallback, matrix );

	cancel = XtVaCreateManagedWidget( "cancel"
			, xmPushButtonWidgetClass
			, buttons
			, NULL );

	XtAddCallback( cancel, XmNactivateCallback, cancelCallback, matrix );

	XtRealizeWidget( toplevel );
	XtAppMainLoop( app );

	return 0;
}

static void commitCallback( Widget w
		, XtPointer client_data, XtPointer call_data )
{
	Widget matrix = (Widget) client_data;
	XbaeMatrixCommitEdit( matrix, True );
}

static void cancelCallback( Widget w
		, XtPointer client_data, XtPointer call_data )
{
	Widget matrix = (Widget) client_data;
	XbaeMatrixCancelEdit( matrix, True );
}

static void matrixCallback( Widget w
		, XtPointer client_data, XtPointer call_data )
{
	String message = (String) client_data;
	fprintf( stderr, "%s\n", message );
}
