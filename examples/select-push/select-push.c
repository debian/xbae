/*
 * AUTHOR: Jay Schmidgall <jay@spdbump.sungardss.com>
 *
 * $Id: select-push.c,v 1.10 2005/04/14 21:01:11 tobiasoed Exp $
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif

#include <stdlib.h>

#ifdef USE_EDITRES
#include <X11/Intrinsic.h>
#include <X11/Xmu/Editres.h>
#endif
#include <Xm/Form.h>
#include <Xm/Label.h>

#include <Xbae/Matrix.h>

static String fallback[] = {
	"Select-push*mw.gridType:		grid_row_shadow",
	"Select-push*mw.cellShadowType:		shadow_out",
	"Select-push*mw.cellShadowThickness:	2",
	"Select-push*mw.rows:			10",
	"Select-push*mw.columns:		10",
	"Select-push*mw.visibleRows:		5",
	"Select-push*mw.visibleColumns:		5",
	"Select-push*mw.fixedRows:		1",
	"Select-push*mw.fixedColumns:		1",
	"Select-push*mw.trailingFixedRows:	1",
	"Select-push*mw.trailingFixedColumns:	1",
	"Select-push*mw.columnWidths:		8, 8, 8, 8, 8, 8,"
	"					8, 8, 8, 8, 8, 8",
	"Select-push*mw.columnLabels:		Zero, One, Two, Three, Four,"
	"					Five, Six, Seven, Eight, Nine",
	"Select-push*mw.rowLabels:		0, 1, 2, 3, 4, 5, 6, 7, 8, 9",
	"Select-push*mw.translations:		#replace\\n"
	"		Shift<Btn1Down>	:   	EditCell(Pointer)\\n"
	"		<Btn1Down>	:	SelectCell(select)",
	"Select-push*mw.textTranslations:	"
	"		<Key>Tab	:   	SelectCell(cancel)\\n"
	"		<Key>osfActivate	:   	SelectCell(commit)\\n"
	"		<Key>Return	:   	SelectCell(commit)",
    NULL
};

typedef struct {
    int	last_selected;
    Boolean armed;
} SelectedData;

void
LoadMatrix(Widget w)
{
    String *cells[10];
    static String rows[10][10] = {
	{ "0,Zero", "0,One",  "0,Two",    "0,Three",  "0,Four",
	  "0,Five", "0,Six",  "0, Seven", "0, Eight", "0, Nine" },
	{ "1,Zero", "1,One",  "1,Two",    "1,Three",  "1,Four",
	  "1,Five", "1,Six",  "1, Seven", "1, Eight", "1, Nine" },
	{ "2,Zero", "2,One",  "2,Two",    "2,Three",  "2,Four",
	  "2,Five", "2,Six",  "2, Seven", "2, Eight", "2, Nine" },
	{ "3,Zero", "3,One",  "3,Two",    "3,Three",  "3,Four",
	  "3,Five", "3,Six",  "3, Seven", "3, Eight", "3, Nine" },
	{ "4,Zero", "4,One",  "4,Two",    "4,Three",  "4,Four",
	  "4,Five", "4,Six",  "4, Seven", "4, Eight", "4, Nine" },
	{ "5,Zero", "5,One",  "5,Two",    "5,Three",  "5,Four",
	  "5,Five", "5,Six",  "5, Seven", "5, Eight", "5, Nine" },
	{ "6,Zero", "6,One",  "6,Two",    "6,Three",  "6,Four",
	  "6,Five", "6,Six",  "6, Seven", "6, Eight", "6, Nine" },
	{ "7,Zero", "7,One",  "7,Two",    "7,Three",  "7,Four",
	  "7,Five", "7,Six",  "7, Seven", "7, Eight", "7, Nine" },
	{ "8,Zero", "8,One",  "8,Two",    "8,Three",  "8,Four",
	  "8,Five", "8,Six",  "8, Seven", "8, Eight", "8, Nine" },
	{ "9,Zero", "9,One",  "9,Two",    "9,Three",  "9,Four",
	  "9,Five", "9,Six",  "9, Seven", "9, Eight", "9, Nine" }
    };

    cells[0] = &rows[0][0];
    cells[1] = &rows[1][0];
    cells[2] = &rows[2][0];
    cells[3] = &rows[3][0];
    cells[4] = &rows[4][0];
    cells[5] = &rows[5][0];
    cells[6] = &rows[6][0];
    cells[7] = &rows[7][0];
    cells[8] = &rows[8][0];
    cells[9] = &rows[9][0];
    
    XtVaSetValues(w,
		  XmNcells,     cells,
		  NULL);
}

void
cbSelect(Widget w, XtPointer client, XtPointer call)
{
    XbaeMatrixSelectCellCallbackStruct *cbs =
        (XbaeMatrixSelectCellCallbackStruct *) call;
    SelectedData *sd = (SelectedData*) client;

    if (cbs->num_params != 1)
        return;

    if (sd->armed) {
        int row, column;
        XbaeMatrixGetCurrentCell(w, &row, &column);
        XbaeMatrixSetCellShadow(w, row, column, 0);
        sd->armed = False;
    }

    if (strcmp(cbs->params[0], "select") == 0 && cbs->row != -1) {
        if (sd->last_selected == -1) {
            XbaeMatrixSetRowShadow(w, cbs->row, XmSHADOW_IN);
            sd->last_selected = cbs->row;
        } else {
            XbaeMatrixSetRowShadow(w, sd->last_selected, 0);
            if (cbs->row != sd->last_selected) {
                XbaeMatrixSetRowShadow(w, cbs->row, XmSHADOW_IN);
                sd->last_selected = cbs->row;
            } else {
                sd->last_selected = -1;
            }
        }
        XbaeMatrixCancelEdit(w, True);
    } else if (strcmp(cbs->params[0], "cancel") == 0) {
        XbaeMatrixCancelEdit(w, True);
    } else if (strcmp(cbs->params[0], "commit") == 0) {
        XbaeMatrixCommitEdit(w, True);
    }
}

void cbTraverseCell(Widget w, XtPointer client, XtPointer call)
{
    XbaeMatrixTraverseCellCallbackStruct *cbs =
        (XbaeMatrixTraverseCellCallbackStruct *) call;
    SelectedData *sd = (SelectedData*) client;

    if (sd->armed) {
        XbaeMatrixSetCellShadow(w, cbs->row, cbs->column, 0);
        sd->armed = False;
    }
    
    if (strcmp(cbs->param, "LosingFocus") != 0) {
        XbaeMatrixSetCellShadow(w, cbs->next_row, cbs->next_column, XmSHADOW_IN);
        sd->armed = True;
    }
}

int
main(int argc, char *argv[])
{
    Widget toplevel, form, label, mw;
    XmString xms;
    XtAppContext app;
    SelectedData *sd = XtNew(SelectedData);

    sd->last_selected = -1;
    sd->armed = False;

    toplevel = XtVaAppInitialize(&app, "Select-push",
				 NULL, 0,
				 &argc, argv,
				 fallback,
				 NULL);
#ifdef USE_EDITRES
    XtAddEventHandler( toplevel, (EventMask)0, True,
                       _XEditResCheckMessages, NULL);
#endif

    form = XtVaCreateWidget("form", xmFormWidgetClass, toplevel,
			    NULL);

    xms = XmStringCreateLtoR("This matrix mimics a list of radio buttons. Button 1 can be used to \"press\" a row.\nShift-Button 1 allows you to edit a cell, though any cell in a fixed row or column\n is non-editable. Pressing Return accepts the edit. Editing another cell or selecting\nanother row or tabbing out of the cell cancels the edit.", XmFONTLIST_DEFAULT_TAG);
    label = XtVaCreateManagedWidget("label", xmLabelWidgetClass, form,
				    XmNtopAttachment,	XmATTACH_FORM,
				    XmNleftOffset,	4,
				    XmNleftAttachment,	XmATTACH_FORM,
				    XmNrightOffset,	4,
				    XmNrightAttachment,	XmATTACH_FORM,
				    XmNlabelString,	xms,
				    NULL);

    XmStringFree(xms);

    mw = XtVaCreateManagedWidget("mw", xbaeMatrixWidgetClass, form,
				 XmNtopOffset,		4,
				 XmNtopWidget,		label,
				 XmNtopAttachment,	XmATTACH_WIDGET,
				 XmNleftOffset,		4,
				 XmNleftAttachment,	XmATTACH_FORM,
				 XmNrightOffset,	4,
				 XmNrightAttachment,	XmATTACH_FORM,
				 XmNbottomOffset,	4,
				 XmNbottomAttachment,	XmATTACH_FORM,
				 NULL);
    
    XtAddCallback(mw, XmNselectCellCallback, cbSelect, (XtPointer) sd);
    XtAddCallback(mw, XmNtraverseCellCallback, cbTraverseCell, (XtPointer) sd);

    LoadMatrix(mw);

    XtManageChild(form);
    XtRealizeWidget(toplevel);
    XtAppMainLoop(app);

    /*NOTREACHED*/
    return 0;
}
