/* $Header: /cvsroot/xbae/Xbae/examples/tests/dm1.c,v 1.5 2005/04/14 21:01:13 tobiasoed Exp $
   From:        Yasushi Yamasaki <yamapu@osk3.3web.ne.jp>
   To:          lesstif@hungry.com
   Subject:     bug report
   Date:        Sun, 30 Aug 1998 23:55:47 +0900
   Cc:          yamapu@osk3.3web.ne.jp
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <Xm/Xm.h>
#include <Xbae/Matrix.h>

#include <XbaeConfig.h>

static String fallback[] = {
        "XbaeTest.height:           250",
        "XbaeTest.width:            250",
        "XbaeTest*mw.value:         XBAE MATRIX MEMORY FEVER!!!",
        "XbaeTest*mw.columns:       4",
        "XbaeTest*mw.rows:          4",
        "XbaeTest*mw.cells:         1, 2, 3, 4\\n"
        "                           1, 2, 3, 4\\n"
        "                           1, 2, 3, 4\\n"
        "                           1, 2, 3, 4",
        "XbaeTest*mw.rowHeights:     10, 10, 10, 10",
        NULL
};

int
main(int argc, char *argv[])
{
    void *test = XtMalloc(1);

    fprintf(stderr,"XtMalloc(0) returns %s\n", (XtMalloc(0) == NULL) ? "NULL" : "Non null");
    fprintf(stderr,"XtRealloc(NULL,0) returns %s\n", (XtRealloc(NULL, 0) == NULL) ? "NULL" : "Non null");
    fprintf(stderr,"XtRealloc(test,0) returns %s\n", (XtRealloc(test, 0) == NULL) ? "NULL" : "Non null");
    fprintf(stderr,"XtCalloc(0, 0) returns %s\n", (XtCalloc(0, 0) == NULL) ? "NULL" : "Non null");
    fprintf(stderr,"XtCalloc(0, 1) returns %s\n", (XtCalloc(0, 1) == NULL) ? "NULL" : "Non null");
    fprintf(stderr,"XtCalloc(1, 0) returns %s\n", (XtCalloc(1, 0) == NULL) ? "NULL" : "Non null");

#ifndef WITH_DMALLOC
    fprintf(stderr, "This test only makes sense when compiled with dmalloc.\n");

    exit(1);
#else
    shell = XtAppInitialize(&app, "XbaeTest", NULL, 0,
                            &argc, argv,
#if 0
                            NULL,
#else
                            fallback,
#endif
                            NULL,
                            0);

    w = XtCreateWidget("mw", xbaeMatrixWidgetClass, shell, NULL, 0);
    XtDestroyWidget(w);
        
#endif
}
