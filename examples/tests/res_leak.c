/* $Header: /cvsroot/xbae/Xbae/examples/tests/res_leak.c,v 1.2 2003/10/20 19:13:47 dannybackx Exp $
   From:        Yasushi Yamasaki <yamapu@osk3.3web.ne.jp>
   To:          lesstif@hungry.com
   Subject:     bug report
   Date:        Sun, 30 Aug 1998 23:55:47 +0900
   Cc:          yamapu@osk3.3web.ne.jp
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <Xm/Xm.h>
#include <Xbae/Matrix.h>

static String fallback[] = {
        "XbaeTest.height:           250",
        "XbaeTest.width:            250",
        "XbaeTest*mw.value:         XBAE MATRIX MEMORY FEVER!!!",
        "XbaeTest*mw.columns:       4",
        "XbaeTest*mw.rows:          4",
        "XbaeTest*mw.cells:         1, 2, 3, 4\\n"
        "                           1, 2, 3, 4\\n"
        "                           1, 2, 3, 4\\n"
        "                           1, 2, 3, 4",
        NULL
};

int
main(int argc, char *argv[])
{
    XtAppContext app;
    Widget shell, w;
    void *before;
    void *after;
    int iter = 0;
    int diff = 0;
    int total = 0;


    shell = XtAppInitialize(&app, "XbaeTest", NULL, 0,
			    &argc, argv,
#if 0
                                NULL,
#else
                                 fallback,
#endif
			 NULL,
			 0);

    while (iter < 201)
    {
	before = sbrk((ptrdiff_t) 0);
	w = XtCreateWidget("mw", xbaeMatrixWidgetClass, shell, NULL, 0);
	XtDestroyWidget(w);
	after = sbrk((ptrdiff_t) 0);
	if ((int)((char *)after - (char *)before) > 0)
	{
	    if (iter != 0)
	    {
		/*
		printf("%i %i %p %i\n", iter, iter - diff, after - before, (after - before) / (iter - diff));
		 */
		total += (int)((char *)after - (char*)before);
	    }
	    diff = iter;
	}
	iter++;
	if(!(iter%100)) printf("loop %d, leaking %i bytes per Create/Destroy\n", iter, total / iter);
    }
    printf("leaking %i bytes per Create/Destroy\n", total / iter);
    exit((int)(total / iter));
}
