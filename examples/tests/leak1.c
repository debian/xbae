#include <Xbae/Matrix.h>
#include <Xm/Text.h>

XtAppContext app;
Widget toplevel, mw = 0;

static String fallback[] = {
        "Matrixwidget.height:           250",
        "Matrixwidget.width:            250",
        "Matrixwidget*mw.value:         XBAE MATRIX MEMORY FEVER!!!",
        "Matrixwidget*mw.columns:       4",
        "Matrixwidget*mw.rows:          4",
        "Matrixwidget*mw.cells:         1, 2, 3, 4\\n"
        "                               1, 2, 3, 4\\n"
        "                               1, 2, 3, 4\\n"
        "                               1, 2, 3, 4\\n"
        "                               1, 2, 3, 4",
	NULL
};

void
timeout(XtPointer cd, XtIntervalId* id)
{
	if(mw) XtDestroyWidget(mw);
    	mw = XtVaCreateManagedWidget("mw",
/*				 xmTextWidgetClass, toplevel, */
				 xbaeMatrixWidgetClass, toplevel,
				 NULL);
    	XtAppAddTimeOut( app, 100, timeout, NULL);
}

int
main(int argc, char *argv[])
{
    toplevel = XtVaAppInitialize(&app, "Matrixwidget",
				 NULL, 0,
				 &argc, argv,
#if 0
				NULL,
#else
				 fallback,
#endif
				 NULL);
    
    XtAppAddTimeOut( app, 100, timeout, NULL);
    XtRealizeWidget(toplevel);
    XtAppMainLoop(app);
    
    /*NOTREACHED*/
    return 0;
}
