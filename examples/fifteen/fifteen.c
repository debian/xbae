/*
 * AUTHOR: Jay Schmidgall <jay.schmidgall@spdbump.sungardss.com>
 *
 * $Id: fifteen.c,v 1.10 2006/05/15 15:57:04 tobiasoed Exp $
 */
#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef USE_EDITRES
#include <X11/Intrinsic.h>
#include <X11/Xmu/Editres.h>
#endif

#include <Xm/Form.h>
#include <Xm/TextF.h>

#include <Xbae/Matrix.h>

static String fallback[] = {
	"Fifteen*mw.gridType:			grid_cell_shadow",
	"Fifteen*mw.cellShadowType:	shadow_out",
	"Fifteen*mw.cellShadowThickness:	1",
	"Fifteen*mw.rows:			4",
	"Fifteen*mw.columns:			4",
	"Fifteen*mw.visibleRows:		4",
	"Fifteen*mw.visibleColumns:		4",
	"Fifteen*mw.columnWidths:		2, 2, 2, 2",
	"Fifteen*mw.columnAlignments:		ALIGNMENT_CENTER, ALIGNMENT_CENTER,"
	"					ALIGNMENT_CENTER, ALIGNMENT_CENTER",
	"Fifteen*mw.traversalOn:		off",
	"Fifteen*mw.translations:		#replace\\n"
	"			<Btn1Down>:	SelectCell()",
	NULL
};

typedef struct {
    int row, column, count;
    Widget text;
} Empty;

void
LoadMatrix(Widget w)
{
    String *cells[4];
    static String rows[4][4] = {
	{ "3",	"1",	"6",	"2"},
	{ "5",  "7",	"15",   "13"},
	{ "4",  "11",	"8",    "9"},
	{ "14", "10",	"12",   ""}
    };

    cells[0] = &rows[0][0];
    cells[1] = &rows[1][0];
    cells[2] = &rows[2][0];
    cells[3] = &rows[3][0];
    
    XtVaSetValues(w, XmNcells, cells, NULL);
}


#define REDRAW_VALUE 0x0

void
cbSelect(Widget w, XtPointer client, XtPointer call)
{
    XbaeMatrixSelectCellCallbackStruct *cbs =
        (XbaeMatrixSelectCellCallbackStruct *) call;
    Empty *empty;

    XtVaGetValues(w, 
                  XmNuserData, (XtPointer) &empty,
                  NULL);

    if (   (   cbs->row == empty->row
	        && (   cbs->column == empty->column - 1
	            || cbs->column == empty->column + 1) )
        || (   cbs->column == empty->column
            && (   cbs->row == empty->row - 1
	            || cbs->row == empty->row + 1) ) ) {
        char buf[20];

        /* Note: if we set cbs->row, cbs->column first, then
        * the value that 'cell' points to becomes the empty
        * string! */
        
        XbaeMatrixSetCell(w, empty->row, empty->column, XbaeMatrixGetCell(w, cbs->row, cbs->column));
        XbaeMatrixSetCell(w, cbs->row, cbs->column, "");
        
        XbaeMatrixSetCellShadow(w, empty->row, empty->column, 0);
        XbaeMatrixSetCellShadow(w, cbs->row, cbs->column, XmSHADOW_IN);

        empty->row = cbs->row;
        empty->column = cbs->column;

        empty->count++;

        /* Check if won */
        if ( 0 == strcmp(XbaeMatrixGetCell(w, 0, 0), "1") &&
             0 == strcmp(XbaeMatrixGetCell(w, 0, 1), "2") &&
             0 == strcmp(XbaeMatrixGetCell(w, 0, 2), "3") &&
             0 == strcmp(XbaeMatrixGetCell(w, 0, 3), "4") &&
             0 == strcmp(XbaeMatrixGetCell(w, 1, 0), "5") &&
             0 == strcmp(XbaeMatrixGetCell(w, 1, 1), "6") &&
             0 == strcmp(XbaeMatrixGetCell(w, 1, 2), "7") &&
             0 == strcmp(XbaeMatrixGetCell(w, 1, 3), "8") &&
             0 == strcmp(XbaeMatrixGetCell(w, 2, 0), "9") &&
             0 == strcmp(XbaeMatrixGetCell(w, 2, 1), "10") &&
             0 == strcmp(XbaeMatrixGetCell(w, 2, 2), "11") &&
             0 == strcmp(XbaeMatrixGetCell(w, 2, 3), "12") &&
             0 == strcmp(XbaeMatrixGetCell(w, 3, 0), "13") &&
             0 == strcmp(XbaeMatrixGetCell(w, 3, 1), "14") &&
             0 == strcmp(XbaeMatrixGetCell(w, 3, 2), "15") )
        {
            XtRemoveCallback(w, XmNselectCellCallback, cbSelect, (XtPointer) NULL);
            sprintf(buf, "Move %d WINS!", empty->count);
        } else {
            sprintf(buf, "Move %d", empty->count);
        }

        XmTextFieldSetString(empty->text, buf);
    }
}


int
main(int argc, char *argv[])
{
    XtAppContext app;
    Widget toplevel, form, mw;
    int rows, columns;
    Empty *empty = XtNew(Empty);

    toplevel = XtVaAppInitialize(&app, "Fifteen",
				 NULL, 0,
				 &argc, argv,
				 fallback,
				 NULL);
#ifdef USE_EDITRES
    XtAddEventHandler( toplevel, (EventMask)0, True,
                       _XEditResCheckMessages, NULL);
#endif

    form = XtVaCreateWidget("form", xmFormWidgetClass, toplevel,
			    NULL);
    
    mw = XtVaCreateManagedWidget("mw", xbaeMatrixWidgetClass, form,
				 XmNtopOffset,	    4,
				 XmNtopAttachment,  XmATTACH_FORM,
				 XmNleftOffset,     4,
				 XmNleftAttachment, XmATTACH_FORM,
				 XmNrightOffset,    4,
				 XmNrightAttachment,XmATTACH_FORM,
				 XmNuserData,	    (XtPointer) empty,
				 NULL);

    empty->text = XtVaCreateManagedWidget(
	"text", xmTextFieldWidgetClass, form,
	XmNvalue,		    "Move 0",
	XmNeditable,		    False,
	XmNcursorPositionVisible, False,
	XmNcolumns,		    8,
	XmNtopOffset,		    4,
	XmNtraversalOn,	    False,
	XmNmarginWidth,	    2,
	XmNmarginHeight,	    2,
	XmNtopWidget,		    mw,
	XmNtopAttachment,	    XmATTACH_WIDGET,
	XmNleftWidget,	    mw,
	XmNleftAttachment,	    XmATTACH_OPPOSITE_WIDGET,
	XmNrightOffset,	    4,
	XmNrightAttachment,	    XmATTACH_FORM,
	XmNbottomOffset,	    4,
	XmNbottomAttachment,	    XmATTACH_FORM,
	NULL);
    
    XtVaGetValues(mw, XmNrows, &rows, XmNcolumns, &columns, NULL);

    empty->row = rows - 1;
    empty->column = columns - 1;
    empty->count = 0;
    
    XbaeMatrixSetCellShadow(mw, empty->row, empty->column, XmSHADOW_IN);
    
    XtAddCallback(mw, XmNselectCellCallback, cbSelect, (XtPointer) NULL);
    
    LoadMatrix(mw);

    XtManageChild(form);
    XtRealizeWidget(toplevel);
    XtAppMainLoop(app);
    /*NOTREACHED*/
    return 0;
}
