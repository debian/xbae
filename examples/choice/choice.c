/*
 * AUTHOR: Jay Schmidgall <jay.schmidgall@spdbump.sungardss.com>
 *
 * $Id: choice.c,v 1.38 2006/05/16 10:52:46 tobiasoed Exp $
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef USE_EDITRES
#include <X11/Intrinsic.h>
#include <X11/Xmu/Editres.h>
#endif

#include <Xm/Form.h>
#include <Xm/Frame.h>
#include <Xm/Label.h>
#include <Xm/RowColumn.h>
#include <Xm/Scale.h>
#include <Xm/PushB.h>
#include <Xm/ToggleB.h>
#include <Xm/TextF.h>
#include <Xm/Text.h>

#include <Xbae/Matrix.h>

#ifdef WITH_DMALLOC
#include <dmalloc.h>
#endif

#define USE_RENDER_TABLE 0
#define VERBOSE 0

#if (XmVERSION < 2)
#define XmUNSPECIFIED_PIXEL     ((Pixel) (~0))
#endif

static String fallback[] = {
    "Choice*mw.rows:          10",
    "Choice*mw.columns:       10",
    "Choice*mw.visibleRows:    5",
    "Choice*mw.visibleColumns: 5",
    "Choice*mw.columnWidths:  14, 14, 14, 7, 7, 7, 7, 7, 7, 7",
    "Choice*mw.rowHeights:     2,  1,  1, 1, 1, 1, 1, 1, 1, 1",
    "Choice*mw.rowLabels:    000,  1,  2, 3, 4, 5, 6, 7, 8, 9",
    "Choice*mw.columnLabels:  Zero, One, Two,   Three, Four,"
    "                         Five, Six, Seven, Eight, Nine",
    "Choice*mw.columnAlignments:"
        "    alignment_beginning, alignment_center, alignment_end,"
        "    alignment_beginning, alignment_beginning, alignment_beginning,"
        "    alignment_beginning, alignment_beginning, alignment_beginning,"
        "    alignment_beginning",
/*    "Choice*mw.fontList:        7x14",    */
    "Choice*mw.traverseFixedCells:    True",
    "Choice*mw.topRow:    0",
    "Choice*mw.evenRowBackground: Red",
    "Choice*mw.oddRowBackground: Blue",
    "Choice*mw.altRowCount: 2",
    "Choice*mw.calcCursorPosition: True",
    "Choice*mw.buttonLabels: True",
    "Choice*mw.rowHeightInPixels: False",
    "Choice*mw.multiLineCell: True",
    "Choice*mw.wrapType: wrap_none",
    "Choice*mw.showArrows: True",
    #if 0
    #if USE_RENDER_TABLE
    "Choice*mw.renderTable: labels",
    "Choice*mw.renderTable.fontType:         FONT_IS_FONT",
    "Choice*mw.renderTable.fontName:        -*-helvetica-medium-r-*-*-10-*-*-*-*-*-*-*",
    "Choice*mw.renderTable.labels.fontType:  FONT_IS_FONT",
    "Choice*mw.renderTable.labels.fontName: -*-helvetica-medium-r-*-*-14-*-*-*-*-*-*-*",
    #else
	"Choice*mw.fontList:		-*-helvetica-bold-r-*-*-10-*-*-*-*-*-*-*",
    "Choice*mw.labelFont:		-*-helvetica-bold-r-*-*-14-*-*-*-*-*-*-*",
    #endif
    #else
    "Choice*form.textRenderTable: labels",
    "Choice*form.textRenderTable.fontType:         FONT_IS_FONT",
    "Choice*form.textRenderTable.fontName:        -*-helvetica-medium-o-*-*-10-*-*-*-*-*-*-*",
    "Choice*form.textRenderTable.labels.fontType:  FONT_IS_FONT",
    "Choice*form.textRenderTable.labels.fontName: -*-helvetica-medium-o-*-*-14-*-*-*-*-*-*-*",
    #endif
    #if 0
    "Choice*mw.sensitive: False",
    #endif
    "Choice*mw.translations:	"
    "                            <Btn1Up>           :   DefaultAction()\\n"
    "                            <Btn1Down>         :   DefaultAction() Label() EditCell(Pointer) SelectCell(cell)\\n"
    "                            Shift<Btn2Down>    :   ResizeColumns()\\n"
    "                            <Btn4Down>         :   ScrollRows(-50)\\n"
    "                            <Btn5Down>         :   ScrollRows( 50)\\n",
	#if 0
	"Choice*mw.textTranslations:	"
    "                            <Key>Return    :   CommitEdit(True)\\n",
    #endif
    NULL
};

void
LoadMatrix(Widget w)
{
    String *cells[10];
    static String rows[10][10] = {
    { "a 0,Zero\nThis column is left aligned", "b 0,One\nThis column is center aligned",  "c 0,Two\nThis column is right aligned",    "d 0,Three",  "e 0,Four", 
    "f 0,Five", "g 0,Six",  "h 0, Seven", "i 0, Eight", "j 0, Nine" },
    { "b 1,Zero", "c 1,One",  "d 1,Two",    "e 1,Three",  "f 1,Four", 
    "g 1,Five", "h 1,Six",  "i 1, Seven", "j 1, Eight", "a 1, Nine" },
    { "c 2,Zero", "d 2,One",  "e 2,Two",    "f 2,Three",  "g 2,Four", 
    "h 2,Five", "i 2,Six",  "j 2, Seven", "a 2, Eight", "b 2, Nine" },
    { "d 3,Zero", "e 3,One",  "f 3,Two",    "g 3,Three",  "h 3,Four", 
    "i 3,Five", "j 3,Six",  "a 3, Seven", "b 3, Eight", "c 3, Nine" },
    { "e 4,Zero", "f 4,One",  "g 4,Two",    "h 4,Three",  "i 4,Four", 
    "j 4,Five", "a 4,Six",  "b 4, Seven", "c 4, Eight", "d 4, Nine" },
    { "f 5,Zero", "g 5,One",  "h 5,Two",    "i 5,Three",  "j 5,Four", 
    "a 5,Five", "b 5,Six",  "c 5, Seven", "d 5, Eight", "e 5, Nine" },
    { "g 6,Zero", "h 6,One",  "i 6,Two",    "j 6,Three",  "a 6,Four", 
    "b 6,Five", "c 6,Six",  "d 6, Seven", "e 6, Eight", "f 6, Nine" },
    { "h 7,Zero", "i 7,One",  "j 7,Two",    "a 7,Three",  "b 7,Four", 
    "c 7,Five", "d 7,Six",  "e 7, Seven", "f 7, Eight", "g 7, Nine" },
    { "i 8,Zero", "j 8,One",  "a 8,Two",    "b 8,Three",  "c 8,Four", 
    "d 8,Five", "e 8,Six",  "f 8, Seven", "g 8, Eight", "h 8, Nine" },
    { "j 9,Zero", "a 9,One",  "b 9,Two",    "c 9,Three",  "d 9,Four", 
    "e 9,Five", "f 9,Six",  "g 9, Seven", "h 9, Eight", "i 9, Nine\nhaha" }
    };

    cells[0] = &rows[0][0];
    cells[1] = &rows[1][0];
    cells[2] = &rows[2][0];
    cells[3] = &rows[3][0];
    cells[4] = &rows[4][0];
    cells[5] = &rows[5][0];
    cells[6] = &rows[6][0];
    cells[7] = &rows[7][0];
    cells[8] = &rows[8][0];
    cells[9] = &rows[9][0];
    
    XtVaSetValues(w,
    	  XmNcells,     cells,
		  NULL);
}

typedef struct {
    Widget  mw;
    char    *resource;
    int	    value;
} SetValueStruct;

typedef struct {
    int value;
    Pixel armColor;
    Pixel foreColor;
    Pixel backColor;
} SelectModeStruct;

struct name_int {
    char *name;
    int value;
};

enum { SelectSelect, SelectHighlight, SelectArm };

/* ================================================================================================
 * The following functions are for 'select mode' buttons
 */

void
UnarmAll(Widget w)
{
    int rows, columns, r, c;
    XtVaGetValues(w,
                  XmNrows, &rows,
                  XmNcolumns, &columns,
                  NULL);

    for (c = 0; c < columns; c++) {
        XbaeMatrixSetColumnShadow(w, c, 0);
    }

    for (r = 0; r < rows; r++) {
        XbaeMatrixSetRowShadow(w, r, 0);
    }
    
    for (r = 0; r < rows; r++) {
        for (c = 0; c < columns; c++) {
            XbaeMatrixSetCellShadow(w, r, c, 0);
            XbaeMatrixSetCellBackground(w, r, c, XmUNSPECIFIED_PIXEL);
        }
    }
}

void
ArmColumn(Widget w, int column, SelectModeStruct *sms)
{
    int rows, columns;
    unsigned char cellShadowType;
    
    XtVaGetValues(w,
                  XmNrows, &rows,
                  XmNcolumns, &columns,
                  XmNcellShadowType, &cellShadowType,
                  NULL);

    switch (cellShadowType)
    {
    case XmSHADOW_IN:
        XbaeMatrixSetColumnShadow(w, column, XmSHADOW_OUT);
        break;

    case XmSHADOW_OUT:
        XbaeMatrixSetColumnShadow(w, column, XmSHADOW_IN);
        break;

    case XmSHADOW_ETCHED_IN:
        XbaeMatrixSetColumnShadow(w, column, XmSHADOW_ETCHED_OUT);
        break;

    case XmSHADOW_ETCHED_OUT:
        XbaeMatrixSetColumnShadow(w, column, XmSHADOW_ETCHED_IN);
        break;
    }

    XbaeMatrixSetColumnBackgrounds(w, column, &sms->armColor, 1);
}

void
ArmRow(Widget w, int row, SelectModeStruct *sms)
{
    int rows, columns;
    unsigned char cellShadowType;
    
    XtVaGetValues(w,
                  XmNrows, &rows,
                  XmNcolumns, &columns,
                  XmNcellShadowType, &cellShadowType,
                  NULL);

    switch (cellShadowType)
    {
    case XmSHADOW_IN:
        XbaeMatrixSetRowShadow(w, row, XmSHADOW_OUT);
        break;

    case XmSHADOW_OUT:
        XbaeMatrixSetRowShadow(w, row, XmSHADOW_IN);
        break;

    case XmSHADOW_ETCHED_IN:
        XbaeMatrixSetRowShadow(w, row, XmSHADOW_ETCHED_OUT);
        break;

    case XmSHADOW_ETCHED_OUT:
        XbaeMatrixSetRowShadow(w, row, XmSHADOW_ETCHED_IN);
        break;
    }

    XbaeMatrixSetRowBackgrounds(w, row, &sms->armColor, 1);
}

void
ArmCell(Widget w, int row, int column, SelectModeStruct *sms)
{
    int rows, columns, r, c;
    unsigned char cellShadowType;

    XtVaGetValues(w,
                  XmNrows, &rows,
                  XmNcolumns, &columns,
                  XmNcellShadowType, &cellShadowType,
                  NULL);

    for (r = 0; r < rows; r++)
    {
        XbaeMatrixSetCellBackground(w, r, column, sms->armColor);
        switch (cellShadowType)
        {
        case XmSHADOW_IN:
            XbaeMatrixSetCellShadow(w, r, column, XmSHADOW_OUT);
            break;

        case XmSHADOW_OUT:
            XbaeMatrixSetCellShadow(w, r, column, XmSHADOW_IN);
            break;

        case XmSHADOW_ETCHED_IN:
            XbaeMatrixSetCellShadow(w, r, column, XmSHADOW_ETCHED_OUT);
            break;

        case XmSHADOW_ETCHED_OUT:
            XbaeMatrixSetCellShadow(w, r, column, XmSHADOW_ETCHED_IN);
            break;
        }
    }

    for (c = 0; c < columns; c++)
    {
        XbaeMatrixSetCellBackground(w, row, c, sms->armColor);
        switch (cellShadowType)
        {
        case XmSHADOW_IN:
            XbaeMatrixSetCellShadow(w, row, c, XmSHADOW_OUT);
            break;

        case XmSHADOW_OUT:
            XbaeMatrixSetCellShadow(w, row, c, XmSHADOW_IN);
            break;

        case XmSHADOW_ETCHED_IN:
            XbaeMatrixSetCellShadow(w, row, c, XmSHADOW_ETCHED_OUT);
            break;

        case XmSHADOW_ETCHED_OUT:
            XbaeMatrixSetCellShadow(w, row, c, XmSHADOW_ETCHED_IN);
            break;
        }
    }
}

void
cbSelect(Widget w, XtPointer client, XtPointer call)
{
    int x, y;
    char buf[200];
    Widget text = (Widget) client;
    XbaeMatrixSelectCellCallbackStruct *cbs =
        (XbaeMatrixSelectCellCallbackStruct*) call;
    SelectModeStruct *sms;
    unsigned char grid;

    XtVaGetValues(w, 
                  XmNuserData, &sms,
                  XmNgridType, &grid, 
                  NULL);

    XbaeMatrixEventToXY(w, cbs->event, &x, &y);
    
    sprintf(buf, "Selected cell %d,%d : %d, %d : event %d, %d",
        cbs->row, cbs->column, x, y,
        cbs->event->xbutton.x,
        cbs->event->xbutton.y);
    
    if (cbs->row >= 0 && cbs->column >= 0 ) {
        switch (sms->value)
        {
        case SelectSelect:
            XbaeMatrixDeselectAll( w );
            XbaeMatrixSelectColumn( w, cbs->column );
            XbaeMatrixSelectRow( w, cbs->row );
            break;

        case SelectHighlight:
            XbaeMatrixUnhighlightAll( w );
            XbaeMatrixHighlightColumn( w, cbs->column );
            XbaeMatrixHighlightRow( w, cbs->row );
            break;

        case SelectArm:
            UnarmAll( w );
            switch (grid)
            {
            case XmGRID_ROW_SHADOW:
                ArmRow( w, cbs->row, sms );
                break;

            case XmGRID_COLUMN_SHADOW:
                ArmColumn( w, cbs->column, sms );
                break;

            default:
                ArmCell( w, cbs->row, cbs->column, sms );
                break;
            }
            break;
        }
    }

    XmTextSetString(text, buf);
}

void
cbSelectMode(Widget w, XtPointer client, XtPointer call)
{
    SelectModeStruct *sms;
    SetValueStruct *svs = (SetValueStruct *) client;

    XtVaGetValues(svs->mw,
                  XmNuserData, &sms,
                  NULL);
    
    if (svs->value != sms->value) {
        switch(sms->value)
        {
        case SelectSelect:
            XbaeMatrixDeselectAll( svs->mw );
            break;
        case SelectHighlight:
            XbaeMatrixUnhighlightAll( svs->mw );
            break;
        case SelectArm:
            UnarmAll(svs->mw);
            break;
        }

        sms->value = svs->value;
    }
}

void
createSelectMode(Widget parent, Widget mw)
{
    Widget frame, frame2, label, option, menu, but, sb;
    SetValueStruct *svs;
    
    frame = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, parent,
	NULL);

    frame2 = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, frame,
	XmNchildType,		    XmFRAME_TITLE_CHILD,
	XmNchildHorizontalAlignment,   XmALIGNMENT_CENTER,
	XmNchildVerticalAlignment,	    XmALIGNMENT_CENTER,
	XmNmarginWidth,		    4,
	NULL);
    
    label = XtVaCreateManagedWidget(
	"Select Mode (not a resource)", xmLabelWidgetClass, frame2,
	NULL);
    
    menu = XmCreatePulldownMenu(frame, "menu", NULL, 0);
    
    but = XmCreatePushButton(menu, "Select", NULL, 0);
    sb = but;
    XtManageChild(but);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->value = SelectSelect;
    XtAddCallback(but, XmNactivateCallback, cbSelectMode, (XtPointer) svs);
    
    but = XmCreatePushButton(menu, "Highlight", NULL, 0);
    XtManageChild(but);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->value = SelectHighlight;
    XtAddCallback(but, XmNactivateCallback, cbSelectMode, (XtPointer) svs);
    
    but = XmCreatePushButton(menu, "Arm", NULL, 0);
    XtManageChild(but);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->value = SelectArm;
    XtAddCallback(but, XmNactivateCallback, cbSelectMode, (XtPointer) svs);
    
    option = XtVaCreateManagedWidget(
    	"option", xmRowColumnWidgetClass, frame,
        XmNisHomogeneous, True,
        XmNrowColumnType, XmMENU_OPTION,
        XmNsubMenuId, menu,
        XmNmenuHistory, sb,
        NULL);
}

/* ================================================================================================
 * The following functions are for the sliders
 * The resources have to be of type Dimension or else the XtVaGetValues will do garbage
 */
void
cbScale(Widget w, XtPointer client, XtPointer call)
{
    XmScaleCallbackStruct *cbs = (XmScaleCallbackStruct *) call;
    SetValueStruct *svs = (SetValueStruct *) client;
    XtVaSetValues(svs->mw, svs->resource, cbs->value, NULL);
}

Widget
createScaleBox(Widget mw, Widget parent, char *name, char *resource, int min, int max, Boolean is_int)
{
    Widget frame, frame2, label, scale;
    SetValueStruct *svs;
    Dimension value;
    int value_int;

    frame = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, parent,
				    NULL);

    frame2 = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, frame,
	XmNchildType,		    XmFRAME_TITLE_CHILD,
	XmNchildHorizontalAlignment,   XmALIGNMENT_CENTER,
	XmNchildVerticalAlignment,	    XmALIGNMENT_CENTER,
	XmNmarginWidth,		    4,
	NULL);

    label = XtVaCreateManagedWidget(
	name, xmLabelWidgetClass, frame2,
	NULL);

    if (is_int) {
        XtVaGetValues(mw, resource, &value_int, NULL);
    } else {
        XtVaGetValues(mw, resource, &value, NULL);
        value_int = value;
    }

    scale = XtVaCreateManagedWidget(
	"scale", xmScaleWidgetClass, frame,
	XmNchildType,   XmFRAME_WORKAREA_CHILD,
	XmNorientation, XmHORIZONTAL,
    XmNshowValue, XmNEAR_SLIDER,
	XmNminimum,	    min,
	XmNmaximum,	    max,
	XmNscaleMultiple, 1,
    XmNvalue, value_int,
	NULL);

    #if VERBOSE
    if (is_int) {
        fprintf(stderr,"%s (int) %d\n", resource, value_int);
    } else {
        fprintf(stderr,"%s (dimension) %hd\n", resource, value);
    }
    #endif

    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = XtNewString(resource);
    XtAddCallback(scale, XmNvalueChangedCallback, cbScale, (XtPointer) svs);

    return scale;
}

/* ================================================================================================
 * The following functions are for the toggle buttons 
 */
void
cbToggle(Widget w, XtPointer client, XtPointer call)
{
    XmToggleButtonCallbackStruct *cbs = (XmToggleButtonCallbackStruct *) call;
    SetValueStruct *svs = (SetValueStruct *) client;
    XtVaSetValues(svs->mw, svs->resource, cbs->set, NULL);
}

Widget
createToggle(Widget mw, Widget parent, char *name, char *resource)
{
    Widget frame, button;
    SetValueStruct *svs;
    Boolean set;

    frame = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, parent, NULL);
    
    XtVaGetValues(mw, resource, &set, NULL);

    button = XtVaCreateManagedWidget(
	name, xmToggleButtonWidgetClass, frame,
	XmNset, set,
	NULL);

    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = XtNewString(resource);
    XtAddCallback(button, XmNvalueChangedCallback, cbToggle, (XtPointer) svs);

    return button;
}

/* ================================================================================================
 * The following functions are for the radio buttons (unused)
 * The resources have to be of type unsigned char or else the XtVaGetValues will do garbage
 */
void
cbRadio(Widget w, XtPointer client, XtPointer call)
{
    XmToggleButtonCallbackStruct *cbs = (XmToggleButtonCallbackStruct *) call;
    SetValueStruct *svs = (SetValueStruct *) client;

    if (cbs->set)
	XtVaSetValues(svs->mw, svs->resource, svs->value, NULL);
}

Widget
createButtonBox(Widget mw, Widget parent, char *name, String resource, struct name_int pairs[], int n_pairs)
{
    Widget frame, frame2, label, rc;
    int p;
    unsigned char value;
    
    frame = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, parent,
	NULL);

    frame2 = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, frame,
	XmNchildType,		    XmFRAME_TITLE_CHILD,
	XmNchildHorizontalAlignment,   XmALIGNMENT_CENTER,
	XmNchildVerticalAlignment,	    XmALIGNMENT_CENTER,
	XmNmarginWidth,		    4,
	NULL);
    
    label = XtVaCreateManagedWidget(
	name, xmLabelWidgetClass, frame2,
	NULL);
    
    rc = XtVaCreateManagedWidget(
	"rc", xmRowColumnWidgetClass, frame,
	XmNchildType,	    XmFRAME_WORKAREA_CHILD,
	XmNadjustLast,	    False,
	XmNradioBehavior,   True,
	XmNradioAlwaysOne,  True,
	XmNnumColumns,	    1,
	XmNorientation,	    XmVERTICAL,
	XmNpacking,	    XmPACK_TIGHT,
	NULL);
    
    XtVaGetValues(mw, resource, &value, NULL);
    #if VERBOSE
    fprintf(stderr, "%s (unsigned char) %d\n",resource, value);
    #endif
    
    for(p = 0; p < n_pairs; p++){
        Widget button;
        SetValueStruct *svs;
        
        button = XtVaCreateManagedWidget(
	    pairs[p].name, xmToggleButtonWidgetClass, rc,
	    XmNset, pairs[p].value == value, 
            NULL);
            
        svs = XtNew(SetValueStruct);
        svs->mw = mw;
        svs->resource = resource;
        svs->value = pairs[p].value;
        XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    }
    
    return rc;
}

/* ================================================================================================
 * The following functions are for the comboBoxes
 * The resources have to be of type unsigned char or else the XtVaGetValues will do garbage
 */

void
cbCombo(Widget w, XtPointer client, XtPointer call)
{
    SetValueStruct *svs = (SetValueStruct *) client;

	XtVaSetValues(svs->mw, svs->resource, svs->value, NULL);
}

void
createComboBox(Widget mw, Widget parent, char *name, String resource, struct name_int pairs[], int n_pairs) {

    Widget frame, frame2, label, option, menu, but, sb;
    SetValueStruct *svs;
    int p;
    unsigned char value;
    
    frame = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, parent,
	NULL);

    frame2 = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, frame,
	XmNchildType,		    XmFRAME_TITLE_CHILD,
	XmNchildHorizontalAlignment,   XmALIGNMENT_CENTER,
	XmNchildVerticalAlignment,	    XmALIGNMENT_CENTER,
	XmNmarginWidth,		    4,
	NULL);
    
    label = XtVaCreateManagedWidget(
	name, xmLabelWidgetClass, frame2,
	NULL);

    XtVaGetValues(mw, resource, &value, NULL);
    #if VERBOSE
    fprintf(stderr, "%s (unsigned char) %d\n",resource, value);
    #endif

	sb = NULL;
    menu = XmCreatePulldownMenu(frame, "menu", NULL, 0);

    for (p = 0; p < n_pairs; p++) {
		but = XmCreatePushButton(menu, pairs[p].name, NULL, 0);
		XtManageChild(but);

        svs = XtNew(SetValueStruct);
        svs->mw = mw;
        svs->resource = XtNewString(resource);
        svs->value = pairs[p].value;

        XtAddCallback(but, XmNactivateCallback, cbCombo, (XtPointer) svs);

        if(pairs[p].value == value) {
            sb = but;
        }
    }

    option = XtVaCreateManagedWidget(
    	"option", xmRowColumnWidgetClass, frame,
        XmNisHomogeneous, True,
        XmNrowColumnType, XmMENU_OPTION,
        XmNsubMenuId, menu,
        XmNmenuHistory, sb,
        NULL);
}

void
createGridType(Widget parent, Widget mw)
{
    String resource = XtNewString(XmNgridType);
    struct name_int resource_values[] = {
            {"XmGRID_CELL_LINE", XmGRID_CELL_LINE}, 
            {"XmGRID_ROW_LINE", XmGRID_ROW_LINE},
            {"XmGRID_COLUMN_LINE" , XmGRID_COLUMN_LINE},
            {"XmGRID_CELL_SHADOW" , XmGRID_CELL_SHADOW},
            {"XmGRID_ROW_SHADOW" , XmGRID_ROW_SHADOW},
            {"XmGRID_COLUMN_SHADOW" , XmGRID_COLUMN_SHADOW},
            {"XmGRID_NONE" , XmGRID_NONE}
    };

    createComboBox(mw, parent, "Grid Type", resource, resource_values, sizeof resource_values/sizeof *resource_values);
}

void
createCellShadowType(Widget parent, Widget mw)
{
    String resource = XtNewString(XmNcellShadowType);
    struct name_int resource_values[] = {
            {"XmSHADOW_IN", XmSHADOW_IN}, 
            {"XmSHADOW_OUT", XmSHADOW_OUT},
            {"XmSHADOW_ETCHED_IN", XmSHADOW_ETCHED_IN},
            {"XmSHADOW_ETCHED_OUT", XmSHADOW_ETCHED_OUT}
    };

    createComboBox(mw, parent, "Cell Shadow Type", resource, resource_values, sizeof resource_values/sizeof *resource_values);
}

void
createShadowType(Widget parent, Widget mw)
{
    String resource = XtNewString(XmNshadowType);
    struct name_int resource_values[] = {
            {"XmSHADOW_IN", XmSHADOW_IN}, 
            {"XmSHADOW_OUT", XmSHADOW_OUT},
            {"XmSHADOW_ETCHED_IN", XmSHADOW_ETCHED_IN},
            {"XmSHADOW_ETCHED_OUT", XmSHADOW_ETCHED_OUT}
    };

    createComboBox(mw, parent, "Shadow Type", resource, resource_values, sizeof resource_values/sizeof *resource_values);
}

void
createPlacement(Widget parent, Widget mw)
{
    String resource = XtNewString(XmNscrollBarPlacement);
    struct name_int resource_values[] = {
            {"XmTOP_LEFT", XmTOP_LEFT}, 
            {"XmTOP_RIGHT", XmTOP_RIGHT}, 
            {"XmBOTTOM_LEFT", XmBOTTOM_LEFT}, 
            {"XmBOTTOM_RIGHT", XmBOTTOM_RIGHT}
    };

    createComboBox(mw, parent, "Scrollbar Placement", resource, resource_values, sizeof resource_values/sizeof *resource_values);
}

void
createVerticalDisplay(Widget parent, Widget mw)
{
    String resource = XtNewString(XmNverticalScrollBarDisplayPolicy);
    struct name_int resource_values[] = {
            {"XmDISPLAY_NONE", XmDISPLAY_NONE}, 
            {"XmDISPLAY_AS_NEEDED", XmDISPLAY_AS_NEEDED}, 
            {"XmDISPLAY_STATIC", XmDISPLAY_STATIC}
    };

    createComboBox(mw, parent, "Vertical Display Policy", resource, resource_values, sizeof resource_values/sizeof *resource_values);
}

void
createHorizontalDisplay(Widget parent, Widget mw)
{
    String resource = XtNewString(XmNhorizontalScrollBarDisplayPolicy);
    struct name_int resource_values[] = {
            {"XmDISPLAY_NONE", XmDISPLAY_NONE}, 
            {"XmDISPLAY_AS_NEEDED", XmDISPLAY_AS_NEEDED}, 
            {"XmDISPLAY_STATIC", XmDISPLAY_STATIC}
    };

    createComboBox(mw, parent, "Horizontal Display Policy", resource, resource_values, sizeof resource_values/sizeof *resource_values);
}

void
createRowLabelAlignement(Widget parent, Widget mw)
{
    String resource = XtNewString(XmNrowLabelAlignment);
    struct name_int resource_values[] = {
            {"XmALIGNMENT_BEGINNING", XmALIGNMENT_BEGINNING}, 
            {"XmALIGNMENT_CENTER", XmALIGNMENT_CENTER}, 
            {"XmALIGNMENT_END", XmALIGNMENT_END}
    };

    createComboBox(mw, parent, "Row Label Alignment", resource, resource_values, sizeof resource_values/sizeof *resource_values);
}

void
createWrapType(Widget parent, Widget mw)
{
    String resource = XtNewString(XmNwrapType);
    struct name_int resource_values[] = {
            {"XbaeWrapNone", XbaeWrapNone}, 
            {"XbaeWrapContinuous", XbaeWrapContinuous}, 
            {"XbaeWrapWord", XbaeWrapWord}
    };

    createComboBox(mw, parent, "Wrap Type", resource, resource_values, sizeof resource_values/sizeof *resource_values);
}

/* ================================================================================================
 * The following functions are for sorting
 */

int compare_rows(Widget w, int r1, int r2, void *data) {
        int column;
        int n_columns = XbaeMatrixNumColumns(w);
        int result = 0;
        int *column_order = data; 
        
        for(column = 0; result == 0 && column < n_columns; column++) {
                String s1 = XbaeMatrixGetCell(w, r1, column_order[column]);
                String s2 = XbaeMatrixGetCell(w, r2, column_order[column]);
                
                result = strcmp(s1, s2);
        }

        return result;
}

int compare_columns(Widget w, int c1, int c2, void *data) {
        int row;
        int n_rows = XbaeMatrixNumRows(w);
        int result = 0;
        int *row_order = data; 
        
        for(row = 0; result == 0 && row < n_rows; row++) {
                String s1 = XbaeMatrixGetCell(w, row_order[row], c1);
                String s2 = XbaeMatrixGetCell(w, row_order[row], c2);
                
                result = strcmp(s1, s2);
        }

        return result;
}

void
sortCB(Widget w, XtPointer client, XtPointer call)
{
        XbaeMatrixLabelActivateCallbackStruct *cbs =
	        (XbaeMatrixLabelActivateCallbackStruct*) call;
        
        if (cbs->row == -1) {
                /* Sort rows */
                int column;
                int n_columns = XbaeMatrixNumColumns(w);
                int *column_order = (int *) client;

                /* Find the position of the column in the sorting order */
                for(column = 0; column < n_columns; column++) {
                        if (column_order[column] == cbs->column) {
                                break;
                        }
                }

                if (column != 0) {
                        /* Make the clicked column the primary sort key */
                        memmove(&column_order[1], &column_order[0], column * sizeof *column_order);
                        column_order[0] = cbs->column;

                        XbaeMatrixSortRows(w, compare_rows, column_order);
                }
        } else {
                /* Sort columns */
                int row;
                int n_rows = XbaeMatrixNumRows(w);
                int *row_order = (int *) client;

                /* Find the position of the row in the sorting order */
                for(row = 0; row < n_rows; row++) {
                        if (row_order[row] == cbs->row) {
                                break;
                        }
                }

                if (row != 0) {
                        /* Make the clicked row the primary sort key */
                        memmove(&row_order[1], &row_order[0], row * sizeof *row_order);
                        row_order[0] = cbs->row;

                        XbaeMatrixSortColumns(w, compare_columns, row_order);
                }
        }
}

void cbDefault(Widget w, XtPointer client, XtPointer call) {
    Widget text = (Widget) client;
    XbaeMatrixDefaultActionCallbackStruct *cbs =
        (XbaeMatrixDefaultActionCallbackStruct*) call;
    char buf[200];
    sprintf(buf, "Double click in cell %d,%d", cbs->row, cbs->column);
    XmTextSetString(text, buf);
}

int
main(int argc, char *argv[])
{
    SelectModeStruct *sms;
    Widget toplevel, form, mw, text;
    Widget combos, sliders, toggles;
    XtAppContext app;

    toplevel = XtVaAppInitialize(&app, "Choice",
				 NULL, 0,
				 &argc, argv,
				 fallback,
				 NULL);
#ifdef USE_EDITRES
    XtAddEventHandler( toplevel, (EventMask)0, True,
                       _XEditResCheckMessages, NULL);
#endif

    form = XtVaCreateWidget("form", xmFormWidgetClass, toplevel,
			    NULL);

    text = XtVaCreateManagedWidget(
	"text", xmTextFieldWidgetClass, form,
	XmNeditable,		False,
	XmNcursorPositionVisible, False,
	XmNleftAttachment,	XmATTACH_FORM,
	XmNleftOffset,	4,
	XmNbottomAttachment,	XmATTACH_FORM,
	XmNbottomOffset,	4,
	XmNrightAttachment,	XmATTACH_FORM,
	XmNrightOffset,	4,
	NULL);

    sms = XtNew(SelectModeStruct);
    sms->value = SelectSelect;

    {
	Widget button = XtVaCreateWidget("temp", xmToggleButtonWidgetClass,
					 form, NULL);
	XtVaGetValues(button,
		      XmNselectColor, &(sms->armColor),
		      XmNforeground, &(sms->foreColor),
		      XmNbackground, &(sms->backColor),
		      NULL);
	XtDestroyWidget(button);
    }
	
    mw = XtVaCreateManagedWidget(
	"mw", xbaeMatrixWidgetClass, form,
	XmNuserData,		(XtPointer) sms,
	XmNtopOffset,		4,
	XmNleftAttachment,	XmATTACH_FORM,
	XmNleftOffset,		4,
	XmNbottomWidget,	text,
	XmNbottomAttachment,	XmATTACH_WIDGET,
	XmNbottomOffset,	4,
	XmNrightAttachment,	XmATTACH_OPPOSITE_FORM,
	XmNrightOffset,	4,
	NULL);

    #if 1
        #if 0
        {
            short widths[] =  {0, 140, 140, 0, 70, 70, 70, 70, 70, 70, 70};
            short heights[] = {0,  10,  10, 0, 10, 10, 10, 10, 10, 10, 10};
            XtVaSetValues(mw, 
                          XmNcolumnWidthInPixels, True, 
                          XmNrowHeightInPixels, True, 
                          XmNcolumnWidths, widths, 
                          XmNrowHeights, heights,
                          NULL);
        }
        #else
        {
            short widths[] =  {0, 14, 14, 0, 7, 7, 7, 7, 7, 7, 7};
            short heights[] = {0,  1,  1, 0, 1, 1, 1, 1, 1, 1, 1};
            XtVaSetValues(mw, 
                          XmNcolumnWidthInPixels, False, 
                          XmNrowHeightInPixels, False, 
                          XmNcolumnWidths, widths, 
                          XmNrowHeights, heights,
                          NULL);
        }
        #endif
    #endif
    
    XtAddCallback(mw, XmNselectCellCallback, cbSelect, (XtPointer) text);
    XtAddCallback(mw, XmNdefaultActionCallback, cbDefault, (XtPointer) text);
    
    combos = XtVaCreateManagedWidget(
	"rc", xmRowColumnWidgetClass, form,
	XmNtopAttachment,	XmATTACH_FORM,
	XmNleftAttachment,	XmATTACH_FORM,
	XmNadjustLast,		True,
	XmNorientation,		XmVERTICAL,
	XmNnumColumns,		3,
	XmNpacking,		XmPACK_COLUMN,
	NULL);

    createGridType(combos, mw);
    createCellShadowType(combos, mw);
    createShadowType(combos, mw);
    createPlacement(combos, mw);
    createVerticalDisplay(combos, mw);
    createHorizontalDisplay(combos, mw);
    createRowLabelAlignement(combos, mw);
    createWrapType(combos, mw);

    createSelectMode(combos, mw);

    sliders = XtVaCreateManagedWidget(
	"rc", xmRowColumnWidgetClass, form,
	XmNtopWidget,		combos,
	XmNtopAttachment,	XmATTACH_WIDGET,
	XmNleftAttachment,	XmATTACH_FORM,
	XmNadjustLast,		False,
	XmNorientation,		XmVERTICAL,
	XmNnumColumns,		2,
	XmNpacking,		XmPACK_COLUMN,
	NULL);

    createScaleBox(mw, sliders, "Space", XmNspace, 0, 10, False);
    createScaleBox(mw, sliders, "Text Shadow Thickness", XmNtextShadowThickness, 0, 5, False);
    createScaleBox(mw, sliders, "Cell Shadow Thickness", XmNcellShadowThickness, 0, 10, False);
    createScaleBox(mw, sliders, "Cell Margin Width", XmNcellMarginWidth, 0, 10, False);
    createScaleBox(mw, sliders, "Fixed Rows", XmNfixedRows, 0, 5, False);
    createScaleBox(mw, sliders, "Trailing Fixed Rows", XmNtrailingFixedRows, 0, 5, False);
    
    createScaleBox(mw, sliders, "Atl Row Count", XmNaltRowCount, 0, 5, True);
    createScaleBox(mw, sliders, "Shadow Thickness", XmNshadowThickness,0, 10, False);
    createScaleBox(mw, sliders, "Cell Highlight Thickness", XmNcellHighlightThickness, 0, 4, False);
    createScaleBox(mw, sliders, "Cell Margin Height", XmNcellMarginHeight, 0, 10, False);
    createScaleBox(mw, sliders, "Fixed Columns", XmNfixedColumns, 0, 5, False);
    createScaleBox(mw, sliders, "Trailing Fixed Columns", XmNtrailingFixedColumns, 0, 5, False);

    toggles = XtVaCreateManagedWidget(
	"rc", xmRowColumnWidgetClass, form,
	XmNtopWidget,		combos,
	XmNtopAttachment,	XmATTACH_WIDGET,
        XmNleftWidget,          sliders,
        XmNleftAttachment,      XmATTACH_WIDGET,
	XmNadjustLast,		False,
	XmNorientation,		XmVERTICAL,
	XmNnumColumns,		2,
	XmNpacking,		XmPACK_COLUMN,
	NULL);

    createToggle(mw, toggles, "Fill", XmNfill);
    createToggle(mw, toggles, "Horzizontal Fill", XmNhorzFill);
    createToggle(mw, toggles, "Trailing Attached Right", XmNtrailingAttachedRight);
    createToggle(mw, toggles, "NonFixed Detached Left", XmNnonFixedDetachedLeft);
    createToggle(mw, toggles, "Allow Column Resize", XmNallowColumnResize);
    createToggle(mw, toggles, "Column Width in pixels", XmNcolumnWidthInPixels);

    createToggle(mw, toggles, "Reverse Select", XmNreverseSelect);
    createToggle(mw, toggles, "Text background is cell", XmNtextBackgroundIsCell);
    createToggle(mw, toggles, "Traverse Fixed Cells", XmNtraverseFixedCells);

    createToggle(mw, toggles, "Button Labels", XmNbuttonLabels);
    createToggle(mw, toggles, "Vertical Fill", XmNvertFill);
    createToggle(mw, toggles, "Trailing Attached Bottom", XmNtrailingAttachedBottom);
    createToggle(mw, toggles, "NonFixed Detached Top", XmNnonFixedDetachedTop);
    createToggle(mw, toggles, "Allow Row Resize", XmNallowRowResize);
    createToggle(mw, toggles, "Row Height in pixels", XmNrowHeightInPixels);

    createToggle(mw, toggles, "Show arrows", XmNshowArrows);
    createToggle(mw, toggles, "Multi Line Cell", XmNmultiLineCell);

    LoadMatrix(mw);

    {
        int row, column;
        int n_rows = XbaeMatrixNumRows(mw);
        int n_columns = XbaeMatrixNumColumns(mw);
        int *row_order = (int *) XtMalloc(n_rows * sizeof *row_order);
        int *column_order = (int *) XtMalloc(n_columns * sizeof *column_order);
        for(row = 0; row < n_rows; row++) {
                row_order[row] = row;
        }
        for(column = 0; column < n_columns; column++) {
                column_order[column] = column;
        }
        XtAddCallback(mw, XmNlabelActivateCallback, sortCB, (XtPointer) row_order);
        XtAddCallback(mw, XmNlabelActivateCallback, sortCB, (XtPointer) column_order);
    }

    XtVaSetValues(mw,
		  XmNtopWidget,		sliders,
		  XmNtopAttachment,	XmATTACH_WIDGET,
		  XmNrightAttachment,	XmATTACH_FORM,
		  NULL);

    XtManageChild(form);

    XtRealizeWidget(toplevel);
        
    XtAppMainLoop(app);
    /*NOTREACHED*/
    return 0;
}

