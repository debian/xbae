/* $Id: beeps.c,v 1.3 2001/04/22 19:28:27 amai Exp $ */

/* 
   A code sample for
   SF [ #229550 ] Beeps using a pattern and XbaeInput on Linux.
   submitted by Tobias Oed
   
   "When using an XbaeInput widget that has a pattern defined I get beeps
   when I move the window that contains the widget, as well as when the window 
   looses focus etc... 
   I observe this behaviour on intel686 running linux+lesstif, 
   but not on alpha machines running digital unix+motif."
*/

#include <stdlib.h>
#include <stdio.h>

#include <X11/Intrinsic.h>
#include <Xm/Xm.h>

#include <Xbae/Matrix.h> /* for version info */
#include <Xbae/Input.h>


int
main (int argc, char **argv)
{

  Arg wargs[3];
  int m;
  Widget toplevel, input;

  /* Print run-time version of libXbae to stdout */
  printf("Using Xbae %s\n", XbaeGetVersionTxt());
  printf("This example was build with %s\n", XbaeVersionTxt);

  /* Print compile-time version of libXm to stdout */
  printf("Xbae was build with %s\n", XbaeGetXmVersionTxt());
  printf("and is running with %s\n", XmVERSION_STRING);

  toplevel = XtInitialize (argv[0], "TestXbaeInput", NULL, 0, &argc, argv);

  m = 0;
  XtSetArg (wargs[m], XmNpattern, "[d][d][d][d][d]");
  m++;
  input = XbaeCreateInput (toplevel, "XbaeInput", wargs, m);

  XtManageChild (input);
  XtRealizeWidget (toplevel);
  XtMainLoop ();

  return (EXIT_SUCCESS);
}
