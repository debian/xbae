/*
 * $Id: manycellwidgets.c,v 1.5 2005/12/22 16:59:48 tobiasoed Exp $
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif
#include <stdlib.h>
#include <X11/Intrinsic.h>
#include <Xbae/Matrix.h>
#include <Xm/XmAll.h>

#ifdef USE_EDITRES
#include <X11/Intrinsic.h>
#include <X11/Xmu/Editres.h>
#endif

static String fallback[] = {
	"Matrix*mw.rowLabels:		1, 2, 3, 4, 5, 6",
	"Matrix*mw.allowColumnResize:	True",
	"Matrix*mw.columnWidths:	40, 30, 50, 20",
	"Matrix*cellShadowThickness:	2",
	"Matrix*cellMarginWidth:	0",
	"Matrix*cellMarginHeight:	3",
	"Matrix*gridType:           XmGRID_CELL_SHADOW",
	"Matrix*cellShadowType:		shadow_in",
	"Matrix*rowLabelColor:		Red",
	"Matrix*columnLabelColor:	Blue",
	"Matrix*mw.buttonLabels:	True",
	"Matrix*mw.allowColumnResize:	True",
	"Matrix*img0*labelPixmap:	woman",
	"Matrix*img0*labelType:		XmPIXMAP",
	"Matrix*img1*labelPixmap:	plaid",
	"Matrix*img1*labelType:		XmPIXMAP",
	"Matrix*img2*labelPixmap:	keyboard16",
	"Matrix*img2*labelType:		XmPIXMAP",
	"Matrix*img3*labelPixmap:	mailempty",
	"Matrix*img3*labelType:		XmPIXMAP",
	"Matrix*img4*labelPixmap:	xlogo32",
	"Matrix*img4*labelType:		XmPIXMAP",
	"Matrix*lab3*background:	red",
	NULL
};

void CreateRow(Widget mat, int row)
{
	Widget	toggle, l1, l2;
	char	n[20];

    static char toggle_trans[] = 
        "<Btn1Down>                         : DefaultAction() Arm()\n"
        "<Btn1Up>                           : DefaultAction() Select() Disarm()\n"
        "Ctrl <Key>osfUp                    : EditCell(Up)\n"
        "Ctrl <Key>osfDown                  : EditCell(Down)\n"
        "Ctrl <Key>osfLeft                  : EditCell(Left)\n"
        "Ctrl <Key>osfRight                 : EditCell(Right)\n"
        "<Key>osfPageDown                   : PageDown()\n"
        "<Key>osfPageUp                     : PageUp()\n";
    XtTranslations toggle_translations = XtParseTranslationTable(toggle_trans);

    static char label_trans[] = 
        "<Btn1Down>                         : DefaultAction()\n"
        "<Btn1Up>                           : DefaultAction()\n";
    XtTranslations label_translations = XtParseTranslationTable(label_trans);

    static char text_trans[] = 
        "<Btn1Down>                         : DefaultAction() grab-focus()\n"
        "<Btn1Up>                           : DefaultAction()\n"
        "Ctrl <Key>osfUp                    : EditCell(Up)\n"
        "Ctrl <Key>osfDown                  : EditCell(Down)\n"
        "Ctrl <Key>osfLeft                  : EditCell(Left)\n"
        "Ctrl <Key>osfRight                 : EditCell(Right)\n"
        "<Key>osfPageDown                   : PageDown()\n"
        "<Key>osfPageUp                     : PageUp()\n";
    XtTranslations text_translations = XtParseTranslationTable(text_trans);

    if (row == 1) {
        String rows[][3] = {
            {"hello", "world!", "what's"},
            {"going", "on", "here?"}
        };
        String *cells[] = {rows[0], rows[1]};

        Widget submatrix = XtVaCreateManagedWidget("submatrix", xbaeMatrixWidgetClass, mat,
            XmNrows,		2,
            XmNcolumns,		3,
            XmNcells,       cells,
            NULL);

        XbaeMatrixSetCellWidget(mat, row, 0, submatrix);
    } else {
        sprintf(n, "toggle%d", row);
        toggle = XmCreateToggleButton(mat, n, NULL, 0);
        XtManageChild(toggle);
        XtOverrideTranslations(toggle, toggle_translations);
        XbaeMatrixSetCellWidget(mat, row, 0, toggle);
    }

	sprintf(n, "img%d", row);
	l1 = XmCreateLabel(mat, n, NULL, 0);
    XtManageChild(l1);
    XtOverrideTranslations(l1, label_translations);
    XbaeMatrixSetCellWidget(mat, row, 2, l1);

	sprintf(n, "lab%d", row);
	l2 = XmCreateText(mat, n, NULL, 0);
    XtManageChild(l2);
    XtOverrideTranslations(l2, text_translations);
    XbaeMatrixSetCellWidget(mat, row, 3, l2);
    
	/* XbaeMatrixSetRowHeight(mat, row, 30); */
}

void cbDefault(Widget w, XtPointer client, XtPointer call) {
    XbaeMatrixDefaultActionCallbackStruct *cbs =
        (XbaeMatrixDefaultActionCallbackStruct*) call;
    printf("Double click in cell %d,%d\n", cbs->row, cbs->column);
}

int main(int argc, char *argv[])
{
    Widget toplevel, form, mw, text;
    XtAppContext app;
    int i;

    toplevel = XtVaAppInitialize(&app, "Matrix",
				 NULL, 0,
				 &argc, argv,
				 fallback,
				 NULL);

    #ifdef USE_EDITRES
    XtAddEventHandler( toplevel, (EventMask)0, True,
                       _XEditResCheckMessages, NULL);
    #endif

    form = XtVaCreateWidget("form", xmFormWidgetClass, toplevel,
                            NULL);

    text = XtVaCreateManagedWidget("TraversOutToMe", xmTextFieldWidgetClass, form,
            XmNeditable,		True,
            XmNcursorPositionVisible, False,
            XmNleftAttachment,	XmATTACH_FORM,
            XmNleftOffset,	4,
            XmNrightAttachment,	XmATTACH_FORM,
            XmNrightOffset,	4,
            XmNtopAttachment,	XmATTACH_FORM,
            XmNtopOffset,	4,
            NULL);

    mw = XtVaCreateManagedWidget("mw", xbaeMatrixWidgetClass, form,
		    XmNrows,		5,
		    XmNcolumns,		4,
            XmNleftAttachment,	XmATTACH_FORM,
            XmNleftOffset,	4,
            XmNrightAttachment,	XmATTACH_FORM,
            XmNrightOffset,	4,
            XmNbottomAttachment,	XmATTACH_FORM,
            XmNbottomOffset,	4,
            XmNtopAttachment,	XmATTACH_WIDGET,
            XmNtopWidget,	text,
		    NULL);

    XtAddCallback(mw, XmNdefaultActionCallback, cbDefault, (XtPointer) text);

    for (i=0; i<5; i++)
	    CreateRow(mw, i);

    XtManageChild(form);
    XtRealizeWidget(toplevel);
    XtAppMainLoop(app);
    
    /*NOTREACHED*/
    return 0;
}

