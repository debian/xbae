/*
 * Copyright(c) 1999 Andrew Lister
 *                        All rights reserved
 *
 * Copyright � 2001 by the LessTif Developers
 *
 * Permission to use, copy, modify and distribute this material for
 * any purpose and without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies, and that the name of the author not be used in advertising
 * or publicity pertaining to this material without the specific,
 * prior written permission of an authorized representative of
 * the author.
 *
 * THE AUTHOR MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES, EX-
 * PRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST IN-
 * FRINGEMENT OF PATENTS OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE
 * SOFTWARE IS PROVIDED "AS IS", AND IN NO EVENT SHALL THE AUTHOR OR
 * ANY OF ITS AFFILIATES BE LIABLE FOR ANY DAMAGES, INCLUDING ANY
 * LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES RELAT-
 * ING TO THE SOFTWARE.
 *
 * $Id: pattern.c,v 1.1 2003/10/20 19:12:46 dannybackx Exp $
*/

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#ifdef USE_EDITRES
#include <X11/Intrinsic.h>
#include <X11/Xmu/Editres.h>
#endif
#include <Xm/TextF.h>
#include <Xm/RowColumn.h>
#include <Xm/Frame.h>
#include <Xbae/Matrix.h>
#include <Xbae/Input.h>
#include <Xbae/Caption.h>

static String fallback[] = {
	"Input*XmRowColumn.orientation:		vertical",
	"Input*XmRowColumn.numColumns:		2",
	"Input*XmRowColumn.packing:		pack_column",
	"Input*XbaeCaption.labelPosition:	Top",
	"Input*XbaeCaption.labelAlignment:	Center",
	NULL
};

Widget input;

void
validateCB(Widget w, XtPointer cd, XtPointer cb)
{
    XbaeInputValidateCallbackStruct *cbs =
	(XbaeInputValidateCallbackStruct *)cb;

    (void)printf("Validate '%s' = %s\n", cbs->value, 
		 cbs->doit ? "OK" : "Nope");
}

void
autoFillToggle(Widget w, XtPointer cd, XtPointer cb)
{
    int which = (int)cd;

    if (which)
	XtVaSetValues(input, XmNautoFill, True, NULL);
    else
	XtVaSetValues(input, XmNautoFill, False, NULL);
}

void
convertCaseToggle(Widget w, XtPointer cd, XtPointer cb)
{
    int which = (int)cd;

    if (which)
	XtVaSetValues(input, XmNconvertCase, True, NULL);
    else
	XtVaSetValues(input, XmNconvertCase, False, NULL);
}

void
overwriteModeToggle(Widget w, XtPointer cd, XtPointer cb)
{
    int which = (int)cd;

    if (which)
	XtVaSetValues(input, XmNoverwriteMode, True, NULL);
    else
	XtVaSetValues(input, XmNoverwriteMode, False, NULL);
}

void
alignmentToggle(Widget w, XtPointer cd, XtPointer cb)
{
    int which = (int)cd;

    if (which == 0)
	XtVaSetValues(input, XmNalignment, XmALIGNMENT_BEGINNING, NULL);
    else if (which == 1)
	XtVaSetValues(input, XmNalignment, XmALIGNMENT_CENTER, NULL);
    else
	XtVaSetValues(input, XmNalignment, XmALIGNMENT_END, NULL);
}

void
patternCB(Widget w, XtPointer cd, XtPointer cb)
{
#if 0
    char *pattern = XmTextFieldGetString(w);

    if (*pattern == '\0')	
	XtVaSetValues(input, XmNpattern, NULL, NULL);
    else
	XtVaSetValues(input, XmNpattern, pattern, NULL);

    XtFree(pattern);
#endif
}

int
main(int argc, char *argv[])
{
    Widget toplevel, rc, pattern, cw, radio, frame;
    XtAppContext app;
    XmString true, false, begin, centre, end;

    toplevel = XtVaAppInitialize(&app, "Input",
				 NULL, 0,
				 &argc, argv,
				 fallback,
				 NULL);
#ifdef USE_EDITRES
    XtAddEventHandler( toplevel, (EventMask)0, True,
                       _XEditResCheckMessages, NULL);
#endif
    /*
     * Create a Form to hold everything
     */
    rc = XtVaCreateManagedWidget(
	"rc", xmRowColumnWidgetClass, toplevel,
	NULL);

    cw = XtVaCreateManagedWidget(
	"Enter string", xbaeCaptionWidgetClass, rc,
	NULL);

    frame = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, cw,
	NULL);

    input = XtVaCreateManagedWidget(
	"input", xbaeInputWidgetClass, frame,
	XmNtopAttachment, XmATTACH_FORM,
	XmNleftAttachment, XmATTACH_FORM,
	NULL);

    XtAddCallback(input, XmNvalidateCallback, validateCB, NULL);
    XtVaSetValues(input,
		XmNpattern,		"[-]d[d][d][d][d]",
		XmNautoFill,		True,
		XmNoverwriteMode,	False,
		NULL);

    true = XmStringCreateSimple("True");
    false = XmStringCreateSimple("False");

    cw = XtVaCreateManagedWidget(
	"XmNautofill", xbaeCaptionWidgetClass, rc,
	NULL);

    frame = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, cw,
	NULL);

    radio = XmVaCreateSimpleRadioBox(
	frame, "XmNautoFill", 0, autoFillToggle,
	XmVaRADIOBUTTON, false, NULL, NULL, NULL,
	XmVaRADIOBUTTON, true, NULL, NULL, NULL,
	NULL);

    XtVaSetValues(radio, XmNtraversalOn, False, NULL);

    XtManageChild(radio);

    cw = XtVaCreateManagedWidget(
	"XmNconvertCase", xbaeCaptionWidgetClass, rc,
	NULL);

    frame = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, cw,
	NULL);

    radio = XmVaCreateSimpleRadioBox(
	frame, "XmNconvertCase", 1, convertCaseToggle,
	XmVaRADIOBUTTON, false, NULL, NULL, NULL,
	XmVaRADIOBUTTON, true, NULL, NULL, NULL,
	NULL);

    XtVaSetValues(radio, XmNtraversalOn, False, NULL);

    XtManageChild(radio);

    cw = XtVaCreateManagedWidget(
	"XmNpattern", xbaeCaptionWidgetClass, rc,
	NULL);

    frame = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, cw,
	NULL);

    pattern = XtVaCreateManagedWidget(
	"XmNpattern", xmTextFieldWidgetClass, frame,
	NULL);

    XtAddCallback(pattern, XmNactivateCallback, patternCB, NULL);
    XtAddCallback(pattern, XmNlosingFocusCallback, patternCB, NULL);

    cw = XtVaCreateManagedWidget(
	"XmNoverwriteMode", xbaeCaptionWidgetClass, rc,
	NULL);

    frame = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, cw,
	NULL);

    radio = XmVaCreateSimpleRadioBox(
	frame, "XmNoverwriteMode", 0, overwriteModeToggle,
	XmVaRADIOBUTTON, false, NULL, NULL, NULL,
	XmVaRADIOBUTTON, true, NULL, NULL, NULL,
	NULL);

    XtVaSetValues(radio, XmNtraversalOn, False, NULL);

    XtManageChild(radio);

    cw = XtVaCreateManagedWidget(
	"XmNalignment", xbaeCaptionWidgetClass, rc,
	NULL);

    frame = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, cw,
	NULL);

    begin = XmStringCreateSimple("Begin");
    centre = XmStringCreateSimple("Centre");
    end = XmStringCreateSimple("End");

    radio = XmVaCreateSimpleRadioBox(
	frame, "XmNalignment", 0, alignmentToggle,
	XmVaRADIOBUTTON, begin, NULL, NULL, NULL,
	XmVaRADIOBUTTON, end, NULL, NULL, NULL,
	NULL);

    XtVaSetValues(radio, XmNtraversalOn, False, NULL);

    XtManageChild(radio);

    XmStringFree(true);
    XmStringFree(false);
    XmStringFree(begin);
    XmStringFree(centre);
    XmStringFree(end);

    XtRealizeWidget(toplevel);
    XtAppMainLoop(app);

    /*NOTREACHED*/
    return 0;
}
