/*
 * Copyright(c) 1992 Bell Communications Research, Inc. (Bellcore)
 *                        All rights reserved
 *
 * Copyright � 2001, 2003 by the LessTif Developers
 *
 * Permission to use, copy, modify and distribute this material for
 * any purpose and without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies, and that the name of Bellcore not be used in advertising
 * or publicity pertaining to this material without the specific,
 * prior written permission of an authorized representative of
 * Bellcore.
 *
 * BELLCORE MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES, EX-
 * PRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST IN-
 * FRINGEMENT OF PATENTS OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE
 * SOFTWARE IS PROVIDED "AS IS", AND IN NO EVENT SHALL BELLCORE OR
 * ANY OF ITS AFFILIATES BE LIABLE FOR ANY DAMAGES, INCLUDING ANY
 * LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES RELAT-
 * ING TO THE SOFTWARE.
 *
 * $Header: /cvsroot/xbae/Xbae/examples/tests/xm.c,v 1.10 2005/03/28 03:04:26 tobiasoed Exp $
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif
#include <stdlib.h>
#ifdef USE_EDITRES
#include <X11/Intrinsic.h>
#include <X11/Xmu/Editres.h>
#endif
#include <Xbae/Matrix.h>

#define USE_RENDER_TABLE 0

static String fallback[] = {
	"Matrix*mw.columns:		5",
	"Matrix*mw.rows:		6",
	"Matrix*mw.columnWidths:	8, 3, 10, 10, 10",
	"Matrix*mw.allowColumnResize:	True",
	"Matrix*mw.cells:		Orange, 12, Rough, Inches, Large\\n"
	"				Blue, 323, Smooth, Feet, Medium\\n"
	"				Yellow, 456, Bristly, Meters, Large\\n"
	"				Green, 1, Knobby, Miles, Small\\n"
	"				Pink, 33, Hairy, Quarts, Small\\n"
	"				Black, 7, Silky, Gallons, Small",
	"Matrix*mw.cellBackgrounds:	white, antiquewhite, white, white, antiquewhite\\n"
	"				white, antiquewhite, white, white, antiquewhite\\n"
	"				white, antiquewhite, white, white, antiquewhite\\n"
	"				white, antiquewhite, white, white, antiquewhite\\n"
	"				white, antiquewhite, white, white, antiquewhite\\n"
	"				white, antiquewhite, white, white, antiquewhite",
	"Matrix*mw.colors: 		blue, black, blue, blue, black\\n"
	"				blue, black, blue, blue, black\\n"
	"				blue, black, blue, blue, black\\n"
	"				blue, black, blue, blue, black\\n"
	"				blue, black, blue, blue, black\\n"
	"				blue, black, blue, blue, black",
	"Matrix*mw.columnLabelAlignments:alignment_beginning, alignment_center,"
	"				alignment_center, alignment_beginning,"
	"				alignment_beginning",
	"Matrix*mw.columnAlignments:	alignment_beginning, alignment_end,"
	"				alignment_beginning, alignment_beginning,"
	"				alignment_beginning",
	#if USE_RENDER_TABLE
    "Matrix*renderTable: labels, symbol",
    "Matrix*renderTable.fontType:        FONT_IS_FONT",
    "Matrix*renderTable.fontName:        -*-helvetica-medium-r-*-*-10-*-*-*-*-*-*-*",
    "Matrix*renderTable.labels.fontType:  FONT_IS_FONT",
    "Matrix*renderTable.labels.fontName: -*-helvetica-bold-r-*-*-14-*-*-*-*-*-*-*",
    "Matrix*renderTable.symbol.fontType:  FONT_IS_FONT",
    "Matrix*renderTable.symbol.fontName: -adobe-symbol-medium-r-normal--10-100-75-75-p-61-adobe-fontspecific",
    #else
	"Matrix*fontList:		-*-helvetica-medium-r-*-*-10-*-*-*-*-*-*-*",
    "Matrix*labelFont:		-*-helvetica-bold-r-*-*-14-*-*-*-*-*-*-*,"
                            "-adobe-symbol-medium-r-normal--10-100-75-75-p-61-adobe-fontspecific=symbol",
    #endif
	"Matrix*cellHighlightThickness: 2",
	"Matrix*cellShadowThickness:	2",
	"Matrix*cellMarginWidth:	0",
	"Matrix*cellMarginHeight:	3",
	"Matrix*gridType:		grid_cell_shadow",
	"Matrix*cellShadowType:		shadow_in",
	"Matrix*rowLabelColor:		Red",
	"Matrix*columnLabelColor:	Blue",
	"Matrix*mw.buttonLabels:	True",
	"Matrix*mw.allowColumnResize:	True",
	NULL
};

void
labelCB(Widget mw, XtPointer cd, XtPointer cb)
{
	XbaeMatrixLabelActivateCallbackStruct *cbs =
		( XbaeMatrixLabelActivateCallbackStruct * )cb;

	if( cbs->row_label )
		if( XbaeMatrixIsRowSelected( mw, cbs->row ) )
			XbaeMatrixDeselectRow( mw, cbs->row );
		else
			XbaeMatrixSelectRow( mw, cbs->row );
	else
		if( XbaeMatrixIsColumnSelected( mw, cbs->column ) )
			XbaeMatrixDeselectColumn( mw, cbs->column );
		else
			XbaeMatrixSelectColumn( mw, cbs->column );
}

/*
 * Simple example of loaded Matrix
 */

int
main(int argc, char *argv[])
{
	Widget		toplevel, mw;
	XtAppContext	app;

	String      column_labels[]   = {"Color", "#" , "Texture", "Measure", "Size"};
	XmString	xmcolumn_labels[] = {NULL   , NULL, NULL     , NULL     , NULL  };

	String      row_labels[]   = {"1" , "2" , "3" , "4" , "5" , "6"};
	XmString	xmrow_labels[] = {NULL, NULL, NULL, NULL, NULL, NULL};

    XmString    x;
	char		delta[2];

	toplevel = XtVaAppInitialize(&app, "Matrix",
		NULL, 0,
		&argc, argv,
		fallback,
		NULL);
#ifdef USE_EDITRES
	XtAddEventHandler( toplevel, (EventMask)0, True,
		_XEditResCheckMessages, NULL);
#endif

	delta[0] = 0xD1;
	delta[1] = 0;
#if XmVERSION > 1
	x = XmStringGenerate(delta, NULL, XmCHARSET_TEXT, "symbol");
#else
	x = XmStringCreate (delta, "symbol");
#endif

#if USE_RENDER_TABLE
    xmcolumn_labels[2] = XmStringGenerate("Texture ", NULL, XmCHARSET_TEXT, "labels");
    xmrow_labels[2] = XmStringGenerate("3 ", NULL, XmCHARSET_TEXT, "labels");
#else
    xmcolumn_labels[2] = XmStringCreateSimple("Texture ");
    xmrow_labels[2] = XmStringCreateSimple("3");
#endif

	xmcolumn_labels[2] = XmStringConcat(xmcolumn_labels[2], x);
	xmrow_labels[2] = XmStringConcat(xmrow_labels[2], x);
	XmStringFree(x);

	mw = XtVaCreateManagedWidget("mw", xbaeMatrixWidgetClass, toplevel,
		XmNcolumnLabels,	column_labels,
		XmNxmColumnLabels,	xmcolumn_labels,
		XmNrowLabels,	row_labels,
		XmNxmRowLabels,	xmrow_labels,
		NULL);

    XmStringFree(xmcolumn_labels[2]);
    XmStringFree(xmrow_labels[2]);

	XtAddCallback( mw, XmNlabelActivateCallback, ( XtCallbackProc )labelCB,
		NULL );

	XtRealizeWidget(toplevel);
	XtAppMainLoop(app);

	/*NOTREACHED*/
	return 0;
}

